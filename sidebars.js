/**
 * Creating a sidebar enables you to:
 - create an ordered group of docs
 - render a sidebar for each doc of that group
 - provide next/previous navigation

 The sidebars can be generated from the filesystem, or explicitly defined here.

 Create as many sidebars as you want.
 */

// @ts-check

/** @type {import('@docusaurus/plugin-content-docs').SidebarsConfig} */
const sidebars = {
  // By default, Docusaurus generates a sidebar from the docs folder structure
  docSidebar: [
    {
      type: 'category',
      label: 'À propos',
      collapsed: false,
      items: [
        "quick-start",
        "contents",
        {
          type: 'doc',
          id: 'generators',
          label: 'Générateurs'
        },
        "benchmark",
        "features-roadmap",
        "concepts",
        "why-static-websites",
        {
          type: "link",
          href: 'https://yama-cms.com',
          label: 'Pourquoi utiliser Yama CMS ?'
        },
      ]
    },
    {
      type: 'category',
      label: 'Guides utilisateur',
      collapsed: true,
      items: [
        'edition/site',
        'edition/user',
        'edition/media',
        'edition/content',
        'edition/catalog',
        'edition/taxonomy',
        'edition/menu',
        'edition/data',
      ]
    },
    {
      type: 'category',
      label: 'Guides développeur',
      collapsed: true,
      items: [
        'guide/content-type',
        'guide/outputs',
        'guide/images',
        'guide/schemas',
        'guide/build-deploy-earthly',
        'guide/unsupported-generator',
        'guide/presets',
        'guide/404error',
      ],
    },
    {
      type: 'category',
      label: 'Cas d\'usages',
      collapsed: true,
      items: [
        'use-case/breadcrumb',
        'use-case/glossary',
        'use-case/markdown-parser',
        'use-case/schema-files',
        'use-case/translations',
      ]
    },
  ],
};

module.exports = sidebars;
