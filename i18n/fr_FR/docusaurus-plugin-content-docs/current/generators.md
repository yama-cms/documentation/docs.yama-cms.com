import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

# Générateurs de sites statiques

Yama CMS se **repose sur l'utilisation de générateurs de sites statiques** pour la production de contenu web.
Un générateur de site statique est un outil qui permet de générer un site web en utilisant du code et des fichiers préexistants qui sont convertis en pages HTML, CSS et JavaScript.

A l'heure de la sobriété numérique, la transition vers des **sites statiques peut contribuer à la réduction de la consommation d'énergie** et les émissions de carbone associées aux sites web.
Les sites statiques sont également performants et offre une sécurité accrue.
Pour en savoir plus, vous pouvez consulter notre [page dédiée aux sites statiques](/docs/why-static-websites).

## Configurer votre générateur de site statique

Les contenus administrés avec Yama CMS sont générés sous la forme de fichiers et envoyés dans votre dépôt de code.
Certains **générateurs de sites statiques sont développés pour consommer directement des fichiers Markdown** mais d'autres nécessitent d'être légèrement adaptés.

Nous mettons à disposition un tutoriel pour chaque générateur de site statique supporté officiellement par la plateforme :

- [Next.js](/docs/generators/next)
- [Hugo](/docs/generators/hugo)
- [Nuxt 2](/docs/generators/nuxt2)
- [Nuxt 3](/docs/generators/nuxt3)
- [Gatsby](/docs/generators/gatsby)
- [Jekyll](/docs/generators/jekyll)

Si votre **générateur de site statique ne fait pas partie de cette liste**, pas de panique.
Vous pouvez suivre notre guide afin de [configurer un générateur de site statique non supporté](/docs/guide/unsupported-generator).

## Projet de démonstration pour votre générateur de site statique

Vous souhaitez simplement tester Yama CMS, vous pouvez utiliser un projet de démonstration exploitant votre générateur de site statique favoris. 
Le code source est disponible sur [Gitlab](https://gitlab.com/yama-cms/examples) et [Github](https://github.com/orgs/Yama-CMS/repositories), il suffit de créer votre dépôt à partir du code source proposé.

<Tabs queryString="git-hosting">
    <TabItem value="gitlab" label="Gitlab">
        <p>
            Sur Gitlab, vous pouvez "fork" un des <a href="https://gitlab.com/yama-cms/examples">dépôts d'exemple</a>.
            Vous pouvez également clôner le dépôt localement puis en changer la branche distante, en suivant les instructions "Push an existing Git repository" sur votre dépôt vide.
        </p>
        <p>
            <img src="/img/generators/fork-gitlab.png" />
        </p>
    </TabItem>
    <TabItem value="github" label="Github">
         <p>
            Sur Github, vous pouvez créer votre dépôt directement à partir d'un des <a href="https://github.com/orgs/Yama-CMS/repositories">dépôts template</a>.
        </p>
        <img src="/img/generators/template-github.png" />
    </TabItem>
</Tabs>
