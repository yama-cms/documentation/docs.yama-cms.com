import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

# Quick Start

Yama CMS à vocation à être une **technologie agnostique** afin de fonctionner de manière transparente avec différentes applications. 
Pour cette raison, vous avez la possibilité d'utiliser votre **générateur de site statique** préféré.
Afin de débuter avec l'outil, vous devez seulement avoir un **dépôt Git avec une branche existante**.

Vous pouvez consulter notre page sur les [générateurs de site statique officiellement supportés](/docs/generators) par la plateforme.
Dans le cas ou celui-ci ne serait pas encore pris en charge, il est possible de suivre notre guide permettant de [configurer un générateur de site non supporté](/docs/guide/unsupported-generator).

## Configurer votre site

Nous pouvons passer à la configuration de votre premier site, [connectez-vous à l'application](https://app.yama-cms.com) et accédez à l'espace "manager" et cliquez sur le bouton **Créer un site** :

![Image](/img/quick-start/manager.png)

### Débuter la configuration de votre site

Un formulaire à plusieurs étapes vous permet de configurer votre site. 
À chaque étape, un panneau d'aide est affiché sur la partie droite pour faciliter la compréhension.

![Image](/img/quick-start/step-1.png)

### Cloner votre dépôt Git

Une fois la première étape terminée, vous pourrez récupérer la **clé publique** générée par l'application pour permettre au CMS d'accéder à votre dépôt.

<Tabs queryString="git-hosting">
    <TabItem value="gitlab" label="Gitlab">
        <p>
            Pour ajouter une clé de déploiement sur GitLab, rendez-vous sur votre dépôt de code puis sélectionnez dans la barre latérale : <b>Settings / Repository</b>, section <b>Deploy keys</b>. Cochez la case "Grant write permissions to this key".
        </p>
        <img src="/img/quick-start/deploy-key-gitlab.png" />
    </TabItem>
    <TabItem value="github" label="Github">
        <p>
            Pour ajouter une clé de déploiement sur Github, cliquez sur l'onglet <b>Settings</b>, menu <b>Deploy keys</b>, puis cliquez sur le bouton <b>Add deploy key</b>. Cochez la case "Allow write access".
        </p>
        <img src="/img/quick-start/deploy-key-github.png" />
    </TabItem>
</Tabs>

:::warning
Yama CMS doit être autorisé à écrire dans votre dépôt pour pouvoir fonctionner.
:::

### Configurer votre backoffice

Vous pouvez automatiser la configuration de Yama CMS par l'intermédiaire d'un système de "presets". Cette fonctionnalité vous offre la [possibilité d'automatiser la configuration de votre backoffice](/docs/guide/presets) (type de contenu, taxonomies,...).
Pour passer à l'étape suivante, vous devez seulement renseigner le générateur qui sera utilisé pour générer votre site.

:::info
Par défaut, une configuration standard est proposée avec :

- Une taxonomie **Catégorie**
- Un type de contenu **Page**
- Un type de contenu **Post** associée à la taxonomie **Catégorie**
:::

### Configurer votre pipeline CI/CD

À cette étape, l'application a cloné votre dépôt. Vous devez désormais configurer le système de CI/CD utilisé pour réaliser les déploiements de votre site.

Nous utilisons [Earthly](https://docs.earthly.dev/) qui est un outil d'automatisation de pipelines CI/CD pouvant être exécuté en local ou sur les principaux systèmes de CI/CD.
Vous n'avez pas besoin de maîtriser le concept de pipeline pour finaliser votre configuration.
Si vous souhaitez [comprendre l'intérêt d'utiliser Earthly en local](/docs/guide/build-deploy-earthly), vous pouvez suivre notre guide.

Une fois cette étape validée, un fichier `Earthfile` spécifique à votre générateur de site sera créé sur votre dépôt de code.

:::info
Nous avons planifié l'implémentation d'un micro service pour prendre en charge l'intégration continue sans consommer vos quotas GitHub ou GitLab.
L'intégration de notre service permettra également d'éviter les étapes de configuration liées à l'utilisation d'un système de CI/CD externe.
:::

#### Système de CI/CD externe

Une fois l'étape de configuration de votre pipeline CI/CD terminée, si vous avez décidé d'utiliser le système de CI/CD de GitLab ou GitHub. Il faudra renseigner les variables fournies par Yama CMS :
- `YAMA_WEBHOOK_TOKEN`
- `YAMA_PREVIEW_CONTAINER_PASSWORD`
- `YAMA_PRODUCTION_CONTAINER_PASSWORD`

<Tabs queryString="git-hosting">
    <TabItem value="gitlab" label="Gitlab">
        <p>
            Pour ajouter des variables sur GitLab, rendez-vous sur votre dépôt de code puis sélectionnez dans la barre latérale : <b>Settings / CI/CD</b>. Trouvez ensuite la section <b>Variables</b>, et cliquez sur le bouton <b>Add variable</b>.
            Attention à bien décocher la checkbox <b>Protect variable</b>.
        </p>
        <img src="/img/quick-start/variables-gitlab.png" />
    </TabItem>
    <TabItem value="github" label="Github">
        <p>
            Pour ajouter des variables sur Github, cliquez sur l'onglet <b>Settings</b> (1), menu <b>Secrets and variables</b> (2), puis cliquez sur le bouton <b>New repository secret</b> (3).
        </p>
        <img src="/img/quick-start/variables-github.png" />
    </TabItem>
</Tabs>

## Publier votre site

Rendez-vous dans l'espace "backoffice" de votre site. Depuis cet écran, vous avez la possibilité de publier votre site en _preview_ ou en _production_. La prévisualisation permet de visionner votre site sans que celui-ci ne soit indexé par les moteurs de recherche. Ce mode de publication permet également de valider le fonctionnement de votre pipeline CI/CD et vérifier en conditions réelles le fonctionnement de votre site.

![Image](/img/quick-start/backoffice.png)

Pour notre exemple, nous testerons seulement la prévisualisation de notre site en cliquant sur le bouton **Publier**.
Si votre dépôt est correctement configuré, la pipeline devrait se lancer afin d'exécuter Earthly.
Vous serez informé de la progression de la tâche de construction de votre site avec des notifications.
Une fois la tâche terminée, vous pourrez visualiser le site en cliquant sur le bouton **Consulter**.

:::info
N'oubliez pas de configurer la zone DNS de votre nom de domaine pour que votre site soit accessible sur internet !
L'adresse IP vers laquelle faire pointer votre domaine est disponible dans l'espace de configuration de votre site.
:::
