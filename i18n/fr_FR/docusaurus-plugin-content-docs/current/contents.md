# Contenus

Pour Yama CMS, la **souveraineté de vos données** est primordiale. 
Pour cette raison, nous avons fait le choix de rendre vos **contenus indépendants** de la plateforme. 
L'ensemble des contenus administrés avec l'application sont générés sous la forme de fichiers et envoyés dans votre dépôt de code.
Vous **conserverez ainsi la possibilité de reconstruire votre site** et gérer son hébergement comme bon vous semble.

Vos contenus sont exportés au format Markdown, format accepté par la quasi totalité des générateurs de sites statiques.
De nombreux outils permettent de convertir vers et depuis ce format.
Nous vous invitons à lire notre [guide sur la création d'un type de contenu](/docs/guide/content-type) pour obtenir l'ensemble des informations à ce sujet.

## Créer un contenu via l'interface

Une fois votre site configuré, vous pouvez maintenant gérer les contenus de votre site ! Rendez-vous dans l'espace "backoffice" de votre site et cliquez sur **Page** ou **Post**. Ajoutez un titre et un contenu dans l'éditeur de texte puis enregistrez vos modifications :

![Image editeur](/img/contents/page.png)

En allant sur l'interface web de votre dépôt, vous pourrez voir que votre contenu a été automatiquement généré en un fichier markdown et versionné :

![Markdown](/img/contents/markdown.png)

Ce fichier contient deux sections :
### Les Frontmatter

Un ensemble de métadonnées au format YAML est automatiquement ajoutés en début de fichier. Les frontmatters sont un standard établi dans la Jamstack; grâce à ces métadonnées, votre générateur de site statique pourra par exemple avoir accès au média ou catalogues pouvant être associés à un contenu, à la date de publication, ...

Il est également possible d'ajouter des informations par l'intermédiaire du standard [JSON Schema](https://json-schema.org/) pour définir des données structurées qui seront associées aux contenus.

### Le contenu Markdown

Le contenu de votre fichier est généré au format Markdown, un langage de balisage léger offrant une syntaxe facile à lire et à écrire. Ce langage est également compatible avec la plupart des générateurs de sites statiques existants.

Le standard [GitHub Flavored Markdown](https://github.github.com/gfm/) est supporté par Yama CMS. GFM est une surcouche de [CommonMark](https://commonmark.org/) prenant en charge un lot d'extensions : liste de tâches, tableaux, note de bas de page, ...
