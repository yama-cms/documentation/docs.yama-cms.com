# Contenus

Yama CMS permet la gestion de différents types de contenu, **un type de contenu définit les champs et les attributs** qui peuvent être utilisés pour créer et gérer différents éléments de contenu sur un site. Par exemple, pour un site de blog, vous pouvez avoir un type de contenu appelé "Post" qui comprend des champs tels que le titre de l'article, la date de publication, le contenu principal,... 

Pour notre exemple, nous exploiterons un type de contenu "Page" pour réaliser différentes opérations de gestion courante.

<figure>
    <img alt="Une capture d'écran de la liste des contenus de type Page" src="/img/edition/content/list.png" />
    <figcaption>Liste des avec deux pages déjà créées</figcaption>
</figure>

## Gestion d'une page

Les boutons **Créer** ou **Modifier** vous amènent vers l'interface d'édition. Celle-ci propose 3 onglets : [Informations](#informations), [Configuration](#configuration) et [Personnalisation](#personnalisation). 
À droite se trouve la [Médiathèque](#médiathèque), et à gauche les boutons pour changer la langue en cours d'édition et publier le site.

### Informations

<figure>
    <img alt="Une capture d'écran de l'interface d'édition d'un contenu" src="/img/edition/content/informations.png" />
    <figcaption>Interface d'édition de contenus</figcaption>
</figure>


Cet onglet contient les informations principales de votre page, le titre est le seul champ obligatoire.
Vous pourrez également renseigner le contenu de la page et le mettre en forme depuis l'éditeur via la barre d'outils disponible, ou en intégrant des médias depuis votre médiathèque.

### Configuration

![Image](/img/edition/content/configuration.png) 

Ce formulaire est dédié à la configuration de votre page et le renseignement d'informations additionnelles permettant d'améliorer votre référencement naturel.

La plupart des sites vont exploiter le champ **slug** pour générer l'URL de votre page afin d'être lisible par l'internaute et améliorer le référencement. Vous pouvez ou non renseigner le slug de votre page. Par défaut, celui-ci est renseigné automatiquement à partir du titre de votre page.

:::info
Une fois le contenu créé, le champ **slug** est verrouillé ; vous ne pourrez le modifier qu'après avoir cliqué sur l'icône de verrou à sa droite. 
Modifier ce champ peut entraîner la modification de l'URL de votre contenu, et doit donc être manipulé avec prudence.
:::

Le champ **référence** peut être spécifié lors de la création de votre page mais ne pourra plus être modifié par la suite. Il est en général automatiquement généré et vous n'avez pas besoin de vous en préoccuper. Dans certains cas de figure, ce champ peut être exploité par le développeur.

Vous pouvez aussi associer un média à votre page : si celui-ci est une image, il sera automatiquement redimensionné en fonction des formats spécifiés par le développeur du site. Dans le cas où vous souhaiteriez associer plusieurs médias à votre page, vous devrez utiliser un **[Catalogue](/docs/edition/catalog)**.

:::info
Le développeur du site peut configurer l'utilisation ou non d'un ou plusieurs catalogues et/ou [taxonomies](/docs/edition/taxonomy). Vous aurez alors la possibilité ou non d'associer des taxons à un contenu.
:::

### Personnalisation

Par défaut cet onglet est grisé et inaccessible, celui-ci est mis à disposition par le développeur. Vous trouverez dans cet onglet des champs de formulaire à remplir lorsque votre contenu nécessite la saisie d'informations additionnelles.

## Médiathèque

Si vous avez consulté la page au sujet de la **[médiathèque](/docs/edition/media)**, vous aurez pu observer l'apparition de 3 nouveaux onglets dans le menu :

- Abbrs (Abréviations)
- Defs (Définitions)
- Notes (Notes de bas de page)

### Médias

Vous pouvez intégrer un média directement au sein de votre contenu, il suffit de cliquer sur le bouton **Intégrer** du média ou le glisser / déposer. Son emplacement au sein du contenu est déterminé avec la position de votre curseur dans l'éditeur de texte.

![Image](/img/edition/content/media.png)

:::info
Par défaut, les images ajoutées au contenu ne sont pas redimensionnée. Le développeur peut configurer une largeur et/ou hauteur pour les images, il faudra alors exploiter la fonctionnalité de positionnement de votre image depuis l'éditeur de texte pour appliquer ce paramétrage.
:::

### Abréviations / Définitions

La création des abréviations et des définitions se fait de la même façon, commencez par vous rendre sur l'éditeur de contenu. Si vous souhaitez ajouter une définition sur un mot, il faudra le sélectionner puis cliquer sur l'icône en forme de bulle.
Pour l'exemple, nous allons ajouter une abréviation au mot **gagner** qui vient d'être sélectionné (visible grâce au fond bleu clair) :

![Image](/img/edition/content/abbr-create.png)

Vous n'avez plus cas renseigner l'abréviation dans la zone de texte prévue à cet effet.
Pour les plus perspicaces, vous aurez remarqué l'ajout du mot **gagner** à la barre de recherche pour vous permettre de vérifier si l'abréviation a déjà été spécifiée dans un autre contenu et éventuellement d'utiliser la fonction d'import. Cette fonctionnalité permet de copier la description et réaliser une opération de curation de contenu de manière simplifiée.

![Image](/img/edition/content/abbr-update.png)

Les abréviations comme celle que vous pouvez voir ici en survolant le mot **<abbr title="Content Management System">CMS</abbr>** peuvent être spécifiées afin d'améliorer l'expérience utilisateur.
Les définitions fonctionnent différemment, un mot renseigné en tant que définition se verra ajoutée avec sa description en pied de page.

:::info
Vous pouvez également ajouter une abréviation ou une définition à partir de la zone de recherche dans le cas ou le terme n'est pas encore renseigné.
Un point orange associé à un terme indique qu'il provient d'un autre contenu, vous pouvez simplement l'importer et l'adapter au besoin.
:::

### Notes de bas de page

La procédure ressemble à la création d'une abréviation mais diffère légèrement. 
Il faudra cette fois positionner le curseur à l'emplacement où nous souhaitons ajouter la note, puis cliquer sur l'icône prévue à cet effet. 

Pour cet exemple, nous ajouterons une note de bas de page après le mot **Pour**.
Par défaut, une numérotation automatique est proposée. Au besoin, vous pouvez modifier la numérotation par un terme de votre choix :

![Image](/img/edition/content/footnote.png)

Les notes de bas de page sont utilisées pour citer une référence, une source, soit à disposer des arguments ailleurs que dans le texte, soit à ajouter un commentaire, des éléments d'appareil ou d'apparat critique.
Ces fonctionnalités permettront d'améliorer grandement l'expérience utilisateur et **<abbr title="L'accessibilité du web est la problématique de l'accès aux contenus et services web par les personnes handicapées et plus généralement par tous les utilisateurs">l'accessibilité</abbr>** de votre site, et par conséquence d'optimiser votre référencement pour les moteurs de recherche.

