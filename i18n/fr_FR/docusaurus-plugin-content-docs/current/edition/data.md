# Données

Un jeu de données fait référence à un **ensemble structuré** et utilisées par le site web. 
Il peut contenir divers types de données, tels que du texte ou des fichiers. Les jeux de données sont généralement utilisés pour alimenter les fonctionnalités du site web : une liste de partenaires, avis client,...
Ils jouent un rôle essentiel dans le fonctionnement et l'expérience utilisateur d'un site web.

<figure>
    <img alt="Capture d'écran de l'interface d'édition des data de type Partenaire" src="/img/edition/data/form.png" />
    <figcaption>Interface d'édition de données pour d'un Partenaire</figcaption>
</figure>
