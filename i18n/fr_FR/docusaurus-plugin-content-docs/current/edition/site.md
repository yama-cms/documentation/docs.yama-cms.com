# Site

Yama CMS offre une interface conviviale et simplifiée pour créer, modifier, organiser et publier du contenu, tout en fournissant des fonctionnalités supplémentaires pour optimiser le référencement, assurer la sécurité et les performances du site. 
Vous pouvez ainsi gérer efficacement votre présence en ligne sans nécessiter de compétences techniques.

Vous pouvez accéder à la configuration du site depuis la barre latérale en cliquant sur l'image située en haut à gauche.

![Image](/img/edition/site/form.png)

Depuis cet écran, vous aurez la possibilité de renseigner le titre et la description de votre site.
Vous pouvez également paramétrer la page d'accueil et ajouter des langues dans le cas d'un site multilingue.

Un média peut également être associé au site, l'image sélectionnée apparaîtra dans la barre latérale pour éviter les confusions lors de la gestion de plusieurs sites.
