# Menus
Un menu de navigation offre **une structure et une hiérarchie claires**, permettant aux utilisateurs de trouver rapidement les informations qu'ils recherchent et de se déplacer efficacement à travers le site.
Il permet aux visiteurs de naviguer et d'accéder facilement aux différentes pages du site.

Le fonctionnement de ce module est identique à celui des **[taxonomies](/docs/edition/taxonomy)**, vous retrouverez les mêmes fonctionnalités, avec en plus la possibilité de spécifier des **[liens internes](#lien-interne)** et **[externes](#lien-externe)**.

![Image](/img/edition/menu/form.png)  

## Lien interne

Un lien interne fait référence à un contenu de votre site : il permet de lier un élément du menu à un contenu de manière dynamique. Dans le cas où le _lien permanent_ d'un contenu viendrait à changer, le menu sera automatiquement mis à jour pour refléter ce changement.

<figure>
    <img alt="Capture d'écran de l'interface d'édition de lien interne" src="/img/edition/menu/autocomplete.png" />
    <figcaption>Interface d'édition de lien interne</figcaption>
</figure>

## Lien externe

Cette option doit être sélectionnée si vous souhaitez faire un lien vers un autre site.
Néanmoins, vous pouvez spécifier n'importe quels schémas d'URL :
- mailto:email@hostname.com
- #ancre-de-navigation

:::caution
Dans le cadre d'un site multilingue, il faudra veiller à créer un **lien externe** pour chacune des langues de votre site pour améliorer l'expérience utilisateur.
:::
