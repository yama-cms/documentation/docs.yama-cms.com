# Catalogues

Un catalogue de médias est une collection organisée et présentée de manière structurée de divers types de contenus tels que des images, des vidéos, des fichiers audio ou des documents téléchargeables.

Depuis l'interface d'[édition de contenus](/docs/edition/content), le bouton **Catalogues** en haut à droite vous permet d'ajouter un ou plusieurs catalogues à votre contenu.

:::info
Si le bouton est grisé, c'est que cette fonctionnalité est désactivée pour le type de contenu en cours d'édition. Le développeur doit tout d'abord paramétrer les types de contenus qui peuvent intégrer des catalogues.
:::


![Image](/img/edition/catalog/form.png) 

Pour ajouter des médias au catalogue, vous pouvez glisser / déposer des médias depuis votre médiathèque ou cliquer sur le bouton **Intégrer** disponible sur chaque média. 
Vous pouvez aussi gérer la position d'un élément dans le catalogue pour ordonnancer les médias selon vos besoins.
