# Médiathèque

- [Téléverser](#ajouter-un-média) vos médias
- [Modifier les contenus](#modifier-un-média) associés aux médias
- [Retoucher](#module-dédition-dimages) vos images
- Filtrer vos médias par type ou via une recherche textuelle

## Ajouter un média

Vous pouvez glisser / déposer un média dans la zone noir pointillée ou cliquer sur "Browse" pour ajouter un média à votre médiathèque.
Il est également possible de personnaliser les paramètres de votre médiathèque. Par exemple, vous pouvez configurer le niveau de compression des images qui seront téléversées.

![Image](/img/edition/media/upload.png)

## Modifier un média

Par défaut, le nom du fichier est utilisé pour nommer votre média, il peut être modifié afin de faciliter vos recherches.
La description du média pourra être utilisée comme contenu alternatif afin d'améliorer le référencement et l'accessibilité de votre site.

Lors de l'ajout d'une langue à votre site, il est conseillé d'éditer la description de vos médias pour cette nouvelle langue. Pour cela, vous trouverez dans la barre latérale la langue en cours d'édition, libre à vous de sélectionner une autre langue pour éditer vos traductions.

:::caution
N'oubliez pas de vérifier la langue en cours d'édition avant de modifier les informations d'un média.
:::

## Actions du menu

Pour chaque média un menu synthétique est affiché par défaut, un survol permet l'ouverture complète du menu :

- Copier URL : copier le lien du média
- Intégrer : associer un média dans un formulaire ou au sein d'un contenu
- Retoucher : retoucher une image très simplement
- Éditer : éditer le contenu alternatif d'un média
- Supprimer : supprimer un média de votre médiathèque

![Image](/img/edition/media/menu.png) 

### Module d'édition d'images

Le module d'édition d'images est un composant essentiel qui vous offre l'opportunité de modifier et de personnaliser vos images en ligne. 
Il offre une gamme d'outils et de fonctionnalités qui permettent de retoucher, de transformer et d'améliorer vos images selon vos besoins et préférences.

![Image](/img/edition/media/crop.png)  

- L'onglet "Crop", vous permet de choisir la zone de l'image à exploiter.
- L'onglet "Transform", vous permet d'effectuer des transformations sur l'image (rotation,...).

