# Utilisateurs

À travers ce guide, nous allons principalement aborder le concept de rôles utilisateur qui est largement utilisé dans la conception et le développement de sites web pour **gérer les différents niveaux d'accès et les autorisations accordées aux utilisateurs**. Il s'agit d'un système qui définit les droits et les privilèges accordés à chaque type d'utilisateur en fonction de son rôle ou de sa fonction dans le site.

Vous pouvez accéder à la liste des utilisateurs depuis [l'interface d'édition d'un site](/docs/edition/site) en cliquant sur le bouton **Utilisateurs**. 
Des comptes utilisateur peuvent être créés pour inviter des personnes à gérer les contenus du site.
Un e-mail sera envoyé pour confirmer la création du nouveau compte utilisateur.

![Image](/img/edition/user/list.png)

Les rôles utilisateur permettent de catégoriser les utilisateurs en groupes distincts et de leur attribuer des autorisations spécifiques. Yama CMS permet de configurer les rôles suivants :

- Contributeur : Ce rôle permet seulement de gérer ses contenus, il ne permettra pas de modifier les contenus créer par un autre utilisateur ou de publier le site.
- Auteur : Ce rôle offre la possibilité de gérer l'ensemble des contenus du site et sa publication.
- Éditeur : Ce rôle permet en plus de renseigner les données, taxons et menus du site, il donne également accès aux informations générales au site (titre, description, langues,...) ainsi qu'aux utilisateurs.
- Administrateur : Ce rôle possède les privilèges les plus élevés, il donne accès à la configuration du site (type de contenus / données, taxonomies,...). En général, ce rôle est réservé aux développeurs.

:::info
Ces rôles sont classés de manière hiérarchique, un rôle avec un niveau plus élevé va hériter des privilèges du rôle précédent.
:::

![Image](/img/edition/user/form.png)

En définissant ces rôles et en attribuant les autorisations appropriées à chacun, l'éditeur ou l'administrateur du site web peuvent contrôler l'accès aux différentes fonctionnalités et garantir que les utilisateurs n'ont que les droits nécessaires pour effectuer leurs tâches spécifiques. 
Cela permet également de sécuriser les informations sensibles et de maintenir l'intégrité du contenu.



