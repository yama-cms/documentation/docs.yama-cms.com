# Taxonomies
Les taxonomies permettent de créer des **catégories hiérarchiques (des taxons)** pour organiser vos contenus. Lorsqu'il est appliqué aux sites web, le concept de taxonomie permet de catégoriser et de regrouper le contenu du site de manière logique et cohérente.

La taxonomie d'un site web facilite la navigation et la recherche d'informations pour les utilisateurs. Elle permet de créer une structure claire en définissant des catégories et des sous-catégories pour **organiser les différentes pages et articles du site**. Cela aide les visiteurs à trouver rapidement les informations qui les intéressent et à naviguer de manière plus intuitive.

<figure>
    <img alt="Capture d'écran de l'interface d'édition de taxonomies" src="/img/edition/taxonomy/form.png" />
    <figcaption>Interface d'édition de taxonomies avec des taxons Actualités et Événements</figcaption>
</figure>


## Ordonnancement

Un menu propre à chaque élément permet de réaliser une modification, une suppression, mais aussi gérer les **positions** de vos taxons.
Si vous souhaitez redéfinir l'organisation de votre taxonomie, vous pouvez simplement modifier le taxon parent lors de son édition.

![Image](/img/edition/taxonomy/menu.png)
