# Benchmark

Yama CMS donne l'opportunité aux développeurs de **construire un web plus sobre** en exploitant le générateur de site statique adapté à leurs besoins.
Néanmoins, l'étape de construction du site par l'intermédiaire de la pipeline CI/CD est consommateur de ressources.
De ce point de vue, le **choix de l'outil peut avoir un véritable impact**.

Nous avons trouvé intéressant de réaliser une **étude comparative des temps de "build"** pour les générateurs de sites statiques supportés officiellement par la plateforme.
Pour rappel, l'ensemble des contenus administrés par Yama CMS sont générées sous la forme de fichiers Markdown.
Pour nos tests, le **nombre de pages correspond à la quantité de fichiers Markdown** exploités pour construire le site.

<Benchmark />

## Conditions de test

L'ensemble des générateurs de sites statiques ont été utilisés sur la même machine dans les conditions suivantes :

- Avec la même mise en page
- Avec une configuration minimale
- Avec des fichiers Markdown standards
- Sans comptabiliser l'installation des dépendances

## Conclusion

Le choix d'un outil dépendra avant tout de son usage.
**L'impact énergétique d'un site statique** est principalement situé au niveau de l'exécution de la pipeline CI/CD.
Pour un site nécessitant quelques publications dans l'année, le choix de l'outil ne changera fondamentalement pas les choses.
Concernant un site avec grand nombre de pages et alimenté chaque jour, le choix d'un **générateur qui s'exécute 100x plus vite peut avoir un véritable impact !**

De notre point de vue, voici les deux paramètres à prendre en compte pour choisir un générateur de site statique afin de **proposer un web plus sobre** :

1. Fréquence de mise à jour (nombre de publications)
2. Volumétrie du site (nombre de pages)
