# Pourquoi un site statique ?

Les principaux bénéfices d'avoir un site "statique" plutôt que "dynamique" suivent de leur simplicité technique d'un point de vue opérationnel : ce sont des fichiers pré-générés, et il ne reste plus qu'à les envoyer au client. Ce simple fait offre des avantages entre autres sur :
- **La rapidité** : Un serveur web peut servir des fichiers statiques très rapidement, et la principale latence est alors au niveau de la connexion de l'utilisateur. Ces fichiers peuvent aussi être _mis en cache_ beaucoup plus simplement, et peuvent être distribués entièrement par l'intermédiaire d'un _CDN_.
- **L'éfficacité** : Un serveur web qui sert des pages statiques utilisera moins de resources qu'un serveur web qui doit générer des pages dynamiquement.
- **La sécurité** : Un site statique offre une _surface d'attaque_ réduite.

## L'écosystème JAMStack au service des sites statiques
_JAMStack_ (acronyme de JavaScript, APIs, et Markup) est un ensemble d'outils gravitant autour du **principe architectural des sites statiques** (générateurs, _Headless CMS_, systèmes de _build_, services d'_hébergement_...), suit une croissance significative depuis plusieurs années et continue encore d'évoluer. 
Cet écosystème florissant offre une très grande flexibilité quant aux outils que vous pouvez utiliser, et comment vous pouvez les utiliser ensemble.


# Liens externes au sujet des sites statiques
- ["Sites statiques et dynamiques"](https://fr.wikipedia.org/wiki/Site_web#Sites_statiques_et_dynamiques) sur Wikipédia
- [Jamstack.org](https://jamstack.org/) (en anglais), un point d'entrée pour en apprendre plus sur l'architecture JAMStack
- ["The Benefits of Static Site Generators"](https://gohugo.io/about/benefits/) (en anglais)
- ["Chapter 1. Why Static Sites?"](https://www.oreilly.com/library/view/working-with-static/9781491960936/ch01.html#idm140148241866736) - Working with Static Sites (livre en anglais)
