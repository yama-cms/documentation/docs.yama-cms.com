# Concepts

Cette section de la documentation s'adresse principalement aux développeurs qui viendront utiliser la plateforme pour mettre en place un site pour leur client, et cherche à donner une vue d'ensemble des concepts utilisés au travers de la plateforme Yama CMS.

:::info
En tant qu'utilisateur final d'un site, il n'est pas nécessaire de comprendre ces termes pour éditer les contenus et déployer !
:::

Yama CMS se repose sur des standards et outils libres pour combattre le silotage et pour s'intégrer avec le plus d'outils disponibles dans l'écosystème _JAMStack_.

## Git
Git est un outil de gestion de versions qui permet de suivre les modifications apportées à des fichiers au fil du temps. La plateforme Yama CMS se repose sur Git pour 3 raisons :
- C'est un outil puissant et fiable pour la gestion des fichiers d'un projet (suivi des versions, retour en arrière...).
- C'est un outil standard chez les développeurs, ce qui assure la pérennité des sites gérés avec l'outil. N'importe quel développeur saura comment utiliser la plateforme, ou reprendre votre site si vous décidez d'arrêter d'utiliser Yama CMS.
- De nombreux outils s'interfacent avec Git, ce qui permet de décupler les possibilités d'interactions entre Yama CMS et d'autres solutions techniques.

### Comment ça marche ?
Lorsqu'un contenu est édité sur Yama CMS, la plateforme va "push" la nouvelle version du fichier au format Markdown vers un dépôt de code configuré par le développeur. Ensuite, soit via le système de CI du dépôt git (_Github Actions_, _Gitlab CI_, _Circle CI_, _Travis_...) soit via le système de CI de Yama CMS, le générateur de site statique va "build" le site, puis le mettre en ligne.


## Site statique
Un site statique est un type de site web. Le terme est utilisé pour contraster avec les sites web "dynamiques", qui génèrent les pages à la volée à partir d'une base de données : les sites statiques génèrent les pages une fois, puis les réutilisent à chaque fois que quelqu'un y accède.

Pour mieux comprendre, imaginez un livre : chaque page est déjà créée avec son contenu fixe et ne change pas tant que le livre n'est pas modifié. De la même manière, un site statique est préparé à l'avance et les pages sont prêtes à être affichées dès que quelqu'un y accède.

Les principaux avantages des sites statiques sont :
- **Rapidité** : Puisqu'ils n'ont pas besoin de générer les pages à chaque demande, ils peuvent être plus rapides à charger.
- **Sécurité** : Avec une surface d'attaque plus réduite, ils ont moins de composants qui peuvent être potentiellement vulnérables à des attaques.
- **Légèreté** : En générant les pages en amont, les sites statiques consomment moins de ressources à héberger, et sont donc plus écologiques et économiques.

Il existe de nombreux générateurs de sites statiques, comme [Gatsby](https://www.gatsbyjs.com/), [Jekyll](https://jekyllrb.com/ne), [Next.js](https://nextjs.org/), [Nuxt](https://nuxt.com/) ou [Hugo](https://gohugo.io/). 
Yama CMS utilise un système de "presets" pour s'interfacer avec plusieurs des principaux générateurs et permettre à n'importe quel développeur de créer leur propre "presets" pour utiliser un générateur qui n'est pas supporté nativement par la plateforme.

Voir aussi la page "[Pourquoi un site statique ?](why-static-websites)"

## Markdown
Le format Markdown est un système qui a été créé dans le but de faciliter l'écriture et la lecture des documents sans se soucier des détails techniques de la mise en page.
En Markdown, on stylise du texte en utilisant des symboles spéciaux :

```markdown
# titre
## titre h2
### titre h3
_texte en italique_
**texte en gras**

separateur:
===

> Citations
- listes
- à
- puces
```

Le Markdown s'est établi comme un standard pour la rédaction de documentation, la rédaction de messages sur des plateformes discussion et collaboration, et même pour la rédaction de courriels. C'est un format que tous les générateurs de sites statiques acceptent, et de nombreux outils permettent de convertir vers et depuis ce format.

Le standard [GitHub Flavored Markdown](https://github.github.com/gfm/) est supporté par Yama CMS, GFM est une surcouche de [CommonMark](https://commonmark.org/) prenant en charge un lot d'extensions : liste de tâches, tableaux, note de bas de page,...

## JSON Schema
Un [JSON Schema](https://json-schema.org/) est un moyen de décrire et de valider la structure et le contenu des données au format JSON.
Yama CMS utilise ce standard pour permettre aux développeurs de générer des formulaires à la volée et de valider la donnée entrante.
Par exemple, ce schéma permet de valider un objet de forme `{ "firstName": "Galileo", "lastName: "Galilei" }` :
```json
{
  "title": "Registration form",
  "type": "object",
  "required": [ "firstName", "lastName" ],
  "properties": {
    "firstName": { "type": "string", "title": "First name" },
    "lastName":  { "type": "string", "title": "Last name"  },
    "telephone": { "type": "string", "title": "Telephone", "minLength": 10 }
  }
}
```

## Thumbor
[Thumbor](https://www.thumbor.org/) est une solution open source de manipulation d'images à la volée. Il permet de redimensionner, recadrer, filtrer et modifier des images de manière dynamique en fonction des besoins spécifiques d'une application ou d'un site web.

Yama CMS utilise Thumbor pour proposer un outil de retouche simple d'images, et pour permettre aux développeurs d'adapter n'importe quelle image de l'utilisateur selon l'endroit où elle sera utilisée (par exemple en bannière de site, d'en-tête d'article...)

## Earthly
[Earthly](https://earthly.dev/) est un outil de CI/CD qui se veut "write once, run anywhere" : toute la pipeline CI/CD (du _build_ au déploiement) est déclarée dans un manifeste Earthfile, puis Earthly s'occupe d'exécuter cette pipeline peu importe le système de CI/CD dans lequel Earthly lui-même s'exécute. C'est à dire que le même manifeste Earthfile peut être utilisé et sur la machine d'un développeur "en local", et sur Github Actions, et sur Gitlab CI, et sur Travis, et sur n'importe quelle autre système où Docker est disponible : "write once, run anywhere".

Earthly permet à la plateforme Yama CMS de proposer et de supporter un seul manifeste par générateur, qui est alors utilisable sur n'importe quelle plateforme de CI. Bien entendu, Earthly est complètement optionnel. Les développeurs qui ne souhaitent pas l'utiliser, ou qui ont déjà un système de CI/CD en place, peuvent très bien choisir de configurer leur CI/CD autrement.

Yama CMS propose des manifestes Earthfile pour certains générateurs de sites statiques, et la syntaxe familière (un mélange entre Dockerfiles et Makefiles) permet de facilement les adapter à des besoins spécifiques, ou d'en créer de nouveaux.

## Glossaire

Jamstack
: La JAMStack est une approche architecturale qui promeut un découplage entre l'expérience web, la donnée et la logique métier pour une meilleure flexibilité, scalabilité, maintenabilité et de meilleures performances.

Site dynamique
: Un site dont les pages demandées sont générées à la volée par le serveur, en général en faisant des appels à une base de données.

Site statique 
: À l'inverse d'un site dynamique, un site statique est un site dont toutes les pages ont été générées en amont.

Générateur de site statique 
: Un outil qui transforme la "matière brute" (contenus, données, templates) en fichiers de pages web statiques.

Pipeline CI/CD 
: Une suite d'étapes qui s'exécutent automatiquement dans un environnement dédié, configurée à partir d'un manifeste.

Build 
: L'étape où le générateur de site statique est exécuté pour produire les fichiers.

Déploiement 
: L'étape où les pages web produites à l'étape de _Build_ sont téléversées sur un serveur pour les rendre disponibles via Internet.

Containers 
: Une technologie permettant l'isolation d'un environnement d'exécution pour une application, pour garantir entre autre sa stabilité, portabilité et immutabilité.

Docker 
: Une plateforme permettant d'empaqueter une application et ses dépendances dans un _Conteneur_.
