# Paramétrer des formats d'images

Yama CMS permet de proposer différentes résolutions d'images sans vous préoccuper des fichiers téléversés par vos utilisateurs.
L'intégration d'un site avec des **images optimisées pour différentes tailles d'écran** n'a jamais été aussi simple. 

L'application ne va pas seulement retourner des images aux résolutions différentes, elle va également **servir des fichiers au format WebP en fonction de la compatibilité du navigateur** utilisé par l'internaute.
WebP utilise une compression pour réduire la taille des fichiers image tout en maintenant une qualité visuelle élevée. WebP prend en charge la transparence, il est largement utilisé sur le Web pour réduire le temps de chargement des pages et économiser la bande passante, tout en conservant une qualité d'image satisfaisante.

Cette fonctionnalité présente plusieurs avantages : optimisation de la vitesse de chargement, économie de bande passante, amélioration de l'expérience utilisateur et réduction de la consommation d'énergie. 
En résumé, cela permet d'offrir une **expérience utilisateur optimale sur tous les appareils** et de garantir un fonctionnement efficace du site.

:::info
Yama CMS exploite le logiciel open-source [Thumbor](https://www.thumbor.org/) pour la manipulation d'images à la volée.
Si vous souhaitez arrêter d'utiliser Yama CMS, vous pouvez installer votre propre instance pour continuer à bénéficier de ses avantages.
:::

## Configuration

Vous avez la possibilité de créer des formats d'images lors de la configuration d'un type de contenu / données, taxonomie, menu ou catalogue.
Vous pouvez ajouter des formats en fonction des différents points de rupture de votre site, pour générer des miniatures,...

Pour notre exemple, nous ajouterons un format avec le code `sm` et une largeur maximale :

![Image](/img/guide/images/configuration.png)

:::info
Nous conseillons d'utiliser zéro comme valeur pour la largeur ou la hauteur, Thumbor ajustera automatiquement les dimensions pour préserver le rapport d'aspect original de l'image.
:::

Lors de la génération des fichiers au format JSON ou Markdown, vous aurez accès aux informations (url, traductions, mime,...) de l'image originale et aux formats configurés.
Nous avons ajouté un lien et une image pour la page d'accueil de notre menu de navigation, voici le fichier JSON de sortie :

```json title="data/menus/main.json"
{
    "code": "main",
    "parent": null,
    "media": null,
    "translations": {
        "fr_FR": {
            "name": "Menu principal",
            "slug": null,
            "description": null,
            "url": null
        }
    },
    "children": [
        {
            "code": "6489dd84642ec",
            "parent": "main",
            "media": {
                "url": "http:\/\/localhost\/medias\/e9c4e188d57d824686ed01edf0538f02\/2023-06\/6483322a34cfe442566093.png",
                "mime": "image\/png",
                "formats": {
                    "sm": {
                        "url": "http:\/\/localhost:8080\/5ya3SjUCKWL7j1Aqu2rtznWkOgI=\/0x0:0x0\/512x0\/filters:rotate(0)\/web\/medias\/e9c4e188d57d824686ed01edf0538f02\/2023-06\/6483322a34cfe442566093.png"
                    }
                },
                "translations": {
                    "fr_FR": {
                        "alt": "",
                        "name": "Capture d\u2019\u00e9cran de 2022-01-24 14-57-04"
                    }
                }
            },
            "translations": {
                "fr_FR": {
                    "name": "Accueil",
                    "slug": null,
                    "description": null,
                    "url": "\/"
                }
            },
            "children": []
        }
    ]
}
```

## Workflow

Il est important de détailler le flux opérationnel d'une image au sein de Yama CMS pour comprendre pourquoi vous pouvez seulement paramétrer une hauteur et/ou largeur pour vos formats d'images :

1. L'ajout d'une image dans la médiathèque va entraîner sa compression automatique (via le navigateur) pour diminuer le temps de téléversement du fichier.
2. Ensuite, vous pouvez manipuler l'image par l'intermédiaire d'un module (recadrage & transformation) afin de créer de nouvelles images à la volée qui seront également disponibles dans la médiathèque.
3. Pour terminer, les dimensions spécifiées dans votre configuration seront appliquées en plus des filtres utilisés lors de la manipulation de l'image.

En conclusion, une image qui a été associée à un contenu aura pu être compressée, recadrée, transformée et redimensionnée. 
