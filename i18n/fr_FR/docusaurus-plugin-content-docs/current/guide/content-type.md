# Créer un type de contenu

Par défaut, Yama CMS permet de créer des pages et articles. En fonction des besoins de votre site, il peut être intéressant d'ajouter des types de contenus supplémentaires ou de personnaliser ceux existants.

![Image](/img/guide/content-type/form.png)

Lors de la création d'un nouveau type de contenu, vous allez devoir définir un lot d'informations. Par exemple, le code doit être une chaîne de caractères unique qui sera associée au type de contenu.
Vous pouvez également spécifier un label qui sera un élément d'identification plutôt destiné aux utilisateurs et affiché dans la barre latérale.

Si vous souhaitez donner la possibilité aux utilisateurs de créer un catalogue, il faudra dans un premier temps créer un nouveau catalogue. Ensuite, vous pourrez choisir de l'associer au type de contenu. 
Concernant les taxonomies, le fonctionnement est identique. Une fois la taxonomie créée, il est possible de l'activer.

:::info
Vous pouvez également exploiter un type de contenu afin de modéliser une structure de données complexe en bénéficiant des fonctionnalités (catalogues, taxonomies, éditeur de texte avancé,...) et agréger les données avec un autre type de contenu pour composer des pages complexes.
:::

## Liens permanents

Le fonctionnement des liens permanents nécessite un chapitre détaillé. En effet, vous pouvez définir un modèle qui sera appliqué pour générer vos URLs.
Il est possible de spécifier des paramètres qui seront automatiquement remplacés en fonction des valeurs renseignées lors de la création d'un contenu.
Un paramètre doit être déclaré en étant préfixé par `{` et suffixé avec `}` :

- **reference** : référence d'un contenu, cette valeur est générée automatiquement ou renseignée par l'utilisateur
- **locale** : locale utilisée pour un contenu, vous pouvez filtrer pour obtenir seulement la langue : `{locale|language}`
- **slug** : valeur générée automatiquement à partir du titre ou renseignée par l'utilisateur
- **date** : date de publication d'un contenu, vous pouvez [formater la date](https://www.php.net/manual/fr/datetime.format.php) : `{date|Y-m}`
- **tags** : exploite les mots clés renseignés par l'utilisateur
- **<code_taxonomy>** : le code d'une taxonomie peut être utilisé pour exploiter les taxons renseignés par l'utilisateur 

### Exemples

```yaml
reference: 6486d1295c58c
locale: fr_FR
slug: titre-article 
date: '2012-05-28 14:45'
tags: []
category: 
    - Développement
    - PHP
```

- `/{locale}/{slug}` => /fr_FR/titre-article
- `/{locale|language}/{date|Y-m}/{slug}` => /fr/2012-05/titre-article
- `/{locale}/{tags}/{date}/{slug}.html` => /fr_FR/2012-05-28/titre-article.html
- `{category}/{slug}` => /developpement/php/titre-article

## Fichiers de sortie

Pour comprendre le concept général de [configuration des fichiers de sortie](/docs/guide/outputs), vous pouvez consulter notre guide.
Dans ce chapitre, nous détaillerons seulement les spécificités de configuration pour un type de contenu.

:::info
Une opération sur un contenu va entraîner la génération d'un [fichier Markdown](#fichier-markdown) qui sera automatiquement versionné et envoyé sur votre dépôt de code.
Lorsqu'une nouvelle locale est renseignée pour un contenu, un nouveau fichier Markdown est généré.
:::

Comme pour la gestion des liens permanents, vous pouvez exploiter des paramètres pour générer de manière dynamique le chemin de sortie de votre fichier :
- **code** : code associé au type de contenu
- **date** : date de publication d'un contenu, vous pouvez [formatter la date](https://www.php.net/manual/fr/datetime.format.php) : `{date|Y-m}`
- **locale** : locale utilisée pour un contenu, vous pouvez filtrer pour obtenir seulement la langue : `{locale|language}`
- **slug** : valeur générée automatiquement à partir du titre ou renseignée par l'utilisateur
- **reference** : référence d'un contenu, cette valeur est générée automatiquement ou renseignée par l'utilisateur

## Formats d'images

La création d'un site "responsive" nécessite l'utilisation d'images optimisées en fonction des différentes tailles d'écran.
Yama CMS offre la possibilité de générer des formats d'images pour un type de contenu de manière simplifiée afin d'optimiser le temps de chargement de votre site.
Pour comprendre le concept général de [paramétrage des formats d'images](/docs/guide/images), vous pouvez consulter notre guide.

![Image](/img/guide/content-type/formats.png)

Néanmoins, un point nécessite un éclaircissement. Pour un type de contenu, vous avez la possibilité d'activer le redimensionnement des images affichées dans le contenu.
En effet, l'utilisateur peut intégrer au sein de son contenu des images en provenance de la médiathèque, vous pouvez activer leur redimensionnement de manière automatisée.

## Formulaire personnalisé

Vous pouvez ajouter un formulaire personnalisé directement dans l'interface d'édition d'un contenu. 
L'intégration d'un [JSON Schema](https://json-schema.org/) permet de définir des données structurées qui seront associées aux contenus et accessibles dans vos [frontmatter](#frontmatter).
Pour comprendre le concept général de [gestion des formulaires personnalisés](/docs/guide/schemas), vous pouvez consulter notre guide.

![Image](/img/guide/content-type/schemas.png)

## Fichier Markdown

Les types de contenu sont générés au format Markdown, un choix technique pratique et polyvalent pour écrire des fichiers texte. Il est facile à utiliser, compatible avec le web et offre une structure organisée. Sa portabilité et sa compatibilité avec les systèmes de contrôle de version en font une option idéale pour la documentation et la collaboration.
Markdown est largement utilisé sur le web, et de nombreux générateurs de sites statiques prennent en charge nativement ce format.

### Frontmatter

Lors de la génération d'un fichier Markdown, un ensemble de méta données au format YAML sont ajoutées automatiquement en début de fichier : 

```yaml
locale: fr_FR
slug: azerty
permalink: /fr_FR/2023-05-16/azerty
reference: 6463a9e38bd01
parent: null
date: '2023-05-16'
title: azerty
description: null
excerpt: null
tags: []
media: null
catalogs: null
```

:::info
Vous pouvez [personnaliser le format de sortie](/docs/guide/unsupported-generator) afin de prendre en compte les spécificités de votre générateur de site statique. 
:::

Par exemple, si vous souhaitez associer le taxon **Formation** issu de la taxonomie **Catégorie** qui a pour code `category`, vous obtiendrez le résultat suivant :

```yaml
locale: fr_FR
slug: azerty
permalink: /fr_FR/2023-05-16/azerty
reference: 6463a9e38bd01
parent: null
date: '2023-05-16'
title: azerty
description: null
excerpt: null
tags: []
media: null
catalogs: null
//validate-start
category:
  - Formation
//validate-end
```

Vous pouvez imaginer sur les pages de votre site, l'ajout d'un encart pour faire un appel à l'action via l'implémentation du schéma suivant :

```json
{
  "properties": {
    "action": {
      "type": "object",
      "properties": {
        "title": {
          "type": "string"
        },
        "description": {
          "type": "string"
        },
        "button": {
          "type": "object",
          "properties": {
            "label": {
              "type": "string"
            },
            "url": {
              "type": "string"
            }
          }
        }
      }
    }
  }
}
```

Suite à la saisie des informations via l'interface d'édition d'un contenu dans l'onglet **[Personnalisation](/docs/edition/content#personnalisation)**, les données renseignées seront disponibles depuis vos frontmatter :

```yaml
locale: fr_FR
slug: azerty
permalink: /fr_FR/2023-05-16/azerty
reference: 6463a9e38bd01
parent: null
date: '2023-05-16'
title: azerty
description: null
excerpt: null
tags: []
media: null
catalogs: null
category:
  - Formation
//validate-start
action:
  button:
    label: 'Voir le site'
    url: 'https://yama-cms.com/'
  title: 'Yama CMS'
  description: 'CMS made in Savoie'
//validate-end
```

:::caution
Il faudra porter une attention particulière au nommage de vos données personnalisées pour éviter d'écraser les méta données ajoutées automatiquement par Yama CMS.
:::

### Markdown

Votre parseur **Markdown** devra permettre de conserver les balises HTML. Par exemple, des balises `<figure>` sont générées lors d'ajout d'images au contenu.
L'application injecte une classe à la balise selon le positionnement défini par l'utilisateur, vous pourrez ainsi définir en amont votre **CSS** pour prendre en charge la mise en page :

```md
Lorem ipsum dolor sit amet, consectetur adipiscing elit
-------------------------------------------------------

Suspendisse *consectetur* tempus tortor, nec pellentesque nibh. Cras dictum nibh mattis luctus tincidunt. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque et vestibulum mauris, ac ultrices libero. Curabitur gravida, dolor gravida congue auctor, eros diam luctus augue, vel pharetra dolor orci eu lorem. Pellentesque nec interdum nunc, ut aliquet enim. **Maecenas** vel mauris ullamcorper, dignissim sem eget, posuere mauris. Nunc laoreet ut purus eu condimentum. Duis semper dignissim felis, in sollicitudin nunc blandit a. Donec sit amet suscipit nunc. Suspendisse potenti.

<figure class="image image-style-align-center"><img alt="" src="http://localhost:8080/ClkPSTVwkCbui_XHwelkF6CO6Eo=/0x0:0x0/0x0/filters:rotate(0)/web/medias/e9c4e188d57d824686ed01edf0538f02/2023-06/6483322a34cfe442566093.png"></figure>

### Donec rutrum in est ac tincidunt

 Sed mattis consequat risus sit amet volutpat. Duis et faucibus metus. Fusce ullamcorper, nulla quis porta porttitor, magna ante finibus dolor, cursus feugiat nunc sapien et mi. Mauris sit amet odio massa. Curabitur quis nisi iaculis, venenatis tortor eget, volutpat lacus. Aenean tempus, turpis et pellentesque ultrices, tortor lacus gravida nunc, eu lobortis purus mauris imperdiet elit.

<figure class="image image-style-align-right"><img alt="" src="http://localhost:8080/ClkPSTVwkCbui_XHwelkF6CO6Eo=/0x0:0x0/0x0/filters:rotate(0)/web/medias/e9c4e188d57d824686ed01edf0538f02/2023-06/6483322a34cfe442566093.png"></figure>
```

:::info
Le standard [GitHub Flavored Markdown](https://github.github.com/gfm/) est supporté par Yama CMS, GFM est une surcouche de [CommonMark](https://commonmark.org/) prenant en charge un lot d'extensions : liste de tâches, tableaux, note de bas de page,...
Pour bénéficier de l'ensemble des fonctionnalités proposées par Yama CMS  :

- Autoriser les balises HTML au sein du markdown
- Support du standard [GFM](https://github.github.com/gfm/)
- Gestion des abréviations
- Gestion des listes de définitions
:::
