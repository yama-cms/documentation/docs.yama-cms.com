# Intégrer une page d'erreur 404

Notre infrastructure basée sur [OpenStack](https://www.openstack.org/) impose un nommage particulier pour la page d'erreur 404. En effet, celle-ci doit se trouver à la racine de votre répertoire "public" et nommée `404error.html`. La méthode de génération de cette page dépendra du générateur de site statique.

:::info 
Nous avons planifié le développement d'une fonctionnalité permettant d'exploiter le fichier de votre choix comme page d'erreur 404 afin de vous offrir plus de souplesse.
:::
