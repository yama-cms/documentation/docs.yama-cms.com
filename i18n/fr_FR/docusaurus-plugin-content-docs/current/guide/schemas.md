# Gestion de formulaires personnalisés

Un module est proposé pour créer et personnaliser de manière déclarative des formulaires et valider les données par l'intermédiaire de [JSON Schema](https://json-schema.org/). 
Nous utilisons la librairie [React RJSF](https://github.com/rjsf-team/react-jsonschema-form), également connue sous le nom de React JSON Schema Form qui permet de **générer des formulaires dynamiques** à partir d'un JSON Schema, ce qui simplifie considérablement le processus de création de formulaires complexes.

Un éditeur JSON permet d'implémenter vos schémas et de **visualiser le rendu du formulaire au sein de l'interface**.
Vous avez la possibilité d'ajouter des formulaires personnalisés pour vos types de contenus, catalogues et gérer vos jeux de données.

![Image](/img/guide/schemas/jsonschema.png)

## Type de donnée

Pour illustrer cette fonctionnalité, nous allons implémenter un nouveau type de donnée afin de gérer une liste d'auteurs :

### JSON Schema

```json
{
  "properties": {
    "firstname": {
      "type": "string"
    },
    "lastname": {
      "type": "string"
    },
    "dateOfBirth": {
      "type": "string"
    }
  }
}
```

![Image](/img/guide/schemas/jsonschema-form-1.png)

Vous pouvez modifier les libellés du formulaire et ajouter des champs obligatoires :

```json
{
  "required": ["firstname", "lastname"],
  "properties": {
    "firstname": {
      "type": "string",
      "title": "Prénom"
    },
    "lastname": {
      "type": "string",
      "title": "Nom"
    },
    "dateOfBirth": {
      "type": "string",
      "title": "Date de naissance"
    }
  }
}
```

![Image](/img/guide/schemas/jsonschema-form-2.png)

:::info
Vous pouvez consulter le [site officiel du standard JSON Schema](https://json-schema.org/understanding-json-schema/reference/index.html) pour prendre connaissance des types de propriétés et contraintes pouvant être exploitées.
:::

### UI Schema

Pour améliorer l'expérience utilisateur, vous avez la possibilité de personnaliser le rendu du formulaire par l'intermédiaire d'un UI Schema et modifier le comportement du formulaire généré :

- `ui:widget` permet de spécifier le widget à utiliser, par exemple `"ui:widget": "textarea"` pour utiliser un champ de texte multiligne
- `ui:placeholder` permet de spécifier un texte d'exemple ou d'indication à afficher dans le champ avant que l'utilisateur n'entre une valeur
- `ui:help` permet de fournir une aide contextuelle ou une description supplémentaire pour un champ
- `ui:errors` permet de personnaliser les messages d'erreur associés à un champ

Dans notre cas, nous allons paramétrer le système pour exploiter un type de champ "date" :

```json
{
  "dateOfBirth": {
    "ui:widget": "date"
  }
}
```

![Image](/img/guide/schemas/jsonschema-form-3.png)

:::info
Nous vous invitons à lire la [documentation officielle](https://rjsf-team.github.io/react-jsonschema-form/docs/api-reference/uiSchema) pour prendre connaissance des paramétrages possibles.
:::

### Fichier de sortie

Une fois nos schémas renseignés, nous allons ajouter une nouvelle entrée pour notre nouveau type de donnée.
Suite à la saisie des informations via notre formulaire, un fichier JSON est envoyé sur votre dépôt Git. 
Voir le guide au sujet de la [configuration des fichiers de sortie](/docs/guide/outputs), voici le résultat :

```json title="data/authors.json"
[
  {
    "firstname": "Emmanuel",
    "lastname": "Todd",
    "dateOfBirth": "1951-05-16",
    "translations": []
  }
]  
```

Vous pouvez voir apparaître un attribut `translations` qui n'a jamais été déclaré lors de la définition de notre JSON Schema.
Une transition parfaite pour vous parler des schémas multilingues.

## Type de donnée multilingue

Dans le cas d'un site multilingue, vous pouvez avoir besoin de renseigner des traductions pour une structure de données personnalisées.
Les schémas multilingues permettent de répondre à cette problématique, ils offriront aux utilisateurs la possibilité de renseigner des informations en fonction des langues supportées par votre site.

![Image](/img/guide/schemas/jsonschema-i18n.png)

### JSON Schema

```json
{
  "properties" : {
    "biography": {
      "type": "string",
      "title": "Biographie"
    }
  }
}
```

### UI Schema

```json
{
  "biography": {
    "ui:widget": "textarea" 
  }
}
```

### Formulaire multilingue

Lors de la configuration de vos schémas, vous aurez remarqué qu'il est seulement possible de prévisualiser un formulaire pour un JSON Schema.
Rendez-vous dans l'interface d'édition de votre donnée pour visualiser l'association des différents schémas :

![Image](/img/guide/schemas/jsonschema-i18n-form.png)

### Fichier de sortie

```json title="data/authors.json"
[
  {
    "firstname": "Emmanuel",
    "lastname": "Todd",
    "dateOfBirth": "1951-05-16",
    "translations": {
      "fr_FR": {
        "biography": "Emmanuel Todd, n\u00e9 le 16 mai 1951 \u00e0 Saint-Germain-en-Laye, est un anthropologue, historien et essayiste fran\u00e7ais sp\u00e9cialiste des syst\u00e8mes familiaux et de leur influence sur les soci\u00e9t\u00e9s humaines."
      },
      "en_US": {
        "biography": "Emmanuel Todd is a French historian, anthropologist, demographer, sociologist and political scientist at the National Institute of Demographic Studies (INED) in Paris."
      }
    }
  }
]
```
