# Développer et déployer en local avec Earthly

Yama CMS propose une gestion des _pipelines CI/CD_ commune entre les différents générateurs de sites statiques et plateformes de CI/CD via Earthly.

Earthly utilise le _standard de conteneurs OCI_ pour exécuter un manifeste avec une syntaxe familière, compatible avec n'importe quel langage et sur n'importe quel système (en local sur votre machine, ou dans votre pipeline de CI/CD.) Grâce aux containers et son manifeste déclaratif, Earthly peut automatiquement paralléliser ses builds.

Ce guide explique comment profiter des containers et d'Earthly pour développer et publier en local, peu importe le système d'exploitation, et sans avoir à installer directement les outils nécessaires au générateur de site statique.

:::info
Earthly est entièrement facultatif ! Si vous avez déjà les outils nécessaires d'installé ou que vous avez déjà une pipeline de _CI/CD_ en place, vous n'en avez peut-être pas besoin. Earthly est utile pour tester un générateur ou faire des actions manuelles rapidement, et nous permet de supporter simplement plusieurs systèmes de _CI/CD_ avec un seul manifeste par générateur.
:::

## Installation
Installez Earthly en suivant les instructions sur le [site officiel](https://earthly.dev/get-earthly). Alternativement, vous pouvez lancer Earthly via Docker :

```bash
docker run \
    --tty  \
    --privileged \
    --rm         \
    -v "$PWD":/workspace \
    -v /var/run/docker.sock:/var/run/docker.sock \
    -e NO_BUILDKIT=1 \
    earthly/earthly:v0.7.6 --push +swift-list
```

## Configuration des variables d'environnement
Pour déployer votre site sur OpenStack, vous devez déclarer les variables d'environnement nécessaires (soit dans votre terminal, soit dans un fichier `.secret`). Les valeurs sont disponibles lors de la configuration de votre site dans l'espace "manager" :
```shell
OS_USERNAME="user-abcdEFG12345"
OS_PASSWORD="aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"
OS_AUTH_URL="https://auth.cloud.ovh.net/v3/"
OS_AUTH_VERSION="3"
OS_TENANT_NAME="9029000000000000"
OS_STORAGE_URL="https://storage.gra.cloud.ovh.net/v1/AUTH_bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb"
OS_CONTAINER_NAME="ccccccccccccccccccccc"
```

## Référence des targets Earthly
Nos manifestes définissent au minimum les _targets_ suivantes :
- [`+build`](#build--deploy) génère le site.
- [`+deploy-production`](#build--deploy) génère le site, puis le téléverse dans le bucket configuré.
- [`+deploy-preview`](#build--deploy) génère le site puis le téléverse et ajoute un `robots.txt` pour indiquer aux robots indexeurs des moteurs de recherche que le site ne doit pas être indexé.

Nous proposons aussi des _targets_ pour faciliter les tests et le développement en local :
- [`+dev`](#lancer-un-serveur-de-développement) lance un serveur de developpement, selon les images Docker disponible pour le générateur.
- `+stop-dev` stoppe le serveur de developpement.
- [`+update`](#mettre-à-jour-les-dépendances) installe les dépendances et les manifestes (`bundle install` pour ruby, `npm install` pour js...).
- [`+exec`](#lancer-une-commande) permet de lancer une commande arbitraire.
- [`+shell`](#obtenir-un-shell) ouvre un shell _bash_ temporaire dans l'environnement du générateur.
- `+swift-list` liste le contenu du bucket associé au site.

Chaque _target_ (sauf `+deploy-production` et `+deploy-preview`) génère des _artifacts_ (un ou plusieurs fichiers et dossiers), qui peuvent être sauvegardés en local avec l'option `--push` :
- `earthly --push +build` sauvegardera le site construit par votre générateur.
- `earthly --push +update` sauvegardera les dépendances (`node_modules`...) et les manifestes `Gemfile`, `package.json`).

## Utilisation

### Build & Deploy
Lancez la commande `earthly --push --env-file ./.env +deploy-production`. Earthly se chargera de lancer et d'attendre le résultat de la target `+build` avant de déployer le site.

:::info
Une pipeline de CI/CD suivra à peu près le même plan : Installer Earthly, récupérer les variables d’environnement depuis les _secrets_ configurés dans le dépôt, puis lancer la _target_ voulue (`+deploy-production` ou `+deploy-preview`.)

Yama CMS propose des manifestes pour certains systèmes de CI. Un manifeste est automatiquement ajouté à votre dépôt pendant la configuration; ils sont également [disponible ici](https://gitlab.com/yama-cms/templates/earthly/-/tree/main/manifests/ci-systems). Earthly propose également des guides pour d'autres systèmes de CI [ici](https://docs.earthly.dev/ci-integration/vendor-specific-guides).
:::

### Lancer un serveur de développement
Vous pouvez lancer le serveur de développement dans un container avec la commande `earthly +dev`. Vous pouvez changer le port en ajoutant par exemple `--port=3000`.


```bash
# Make sure dependencies are installed
earthly --push +update

# Start dev server
earthly +dev

# Do some development!

# Later on, stop the dev container
+earthly stop-dev
```

:::caution
Si vous quittez le terminal, Earthly ne stoppera pas le container ! Vous deverez manuellement lancer la commande `earthly +stop-dev` (ou `docker stop earthly-dev-server`).
:::



### Mettre à jour les dépendances
Lancez la commande `earthly --push +update` pour télécharger les dépendances et mettre à jour les paquets selon leur _semantic\_versioning_.

### Lancer une commande
Lancez la commande `earthly --push +exec --command="<cmd>"` pour executer `<cmd>` dans le container de développement. Par exemple, vous pouvez installer une nouvelle dépendance npm avec `earthly --push +exec --command="npm install <package>"`.

### Obtenir un shell
Lancez la commande `earthly --push +shell` pour obtenir un shell dans le container de développement. À noter que seuls les fichiers type "manifeste" (`package.json`, `Gemfile`) seront sauvegardés ; toutes modifications sur d'autres fichiers seront ignorées. Utiilisez la commande `exit` (ou Ctrl+D) pour revenir à votre terminal.

## Modifier son manifeste Earthfile
Tous nos manifestes Earthfile suivent la même structure et sont commentés.
Nous vous invitons à adapter votre Earthfile selon les besoins de votre site (par exemple, pour ajouter une étape de build en plus, ou une variable d'environnement). Si vous avez déjà écrit un Dockerfile ou un Makefile, vous trouverez la syntaxe familière ! Pour le reste, la documentation officielle d'Earthly est disponible à l'adresse https://docs.earthly.dev/.

Si vous souhaitez utiliser Earthly avec un générateur de site statique qui n'est pas encore supporté par Yama CMS, veuillez consulter la page dédiée à la [création d'un manifeste personnalisé](/docs/guide/unsupported-generator).

:::caution
Earthly est encore en phase bêta. Certaines features et patterns pourront changer dans le futur (même si les manifestes resteront rétrocompatibles grâce au header `VERSION`), ou sont encore en état de reflection (par exemple la place du cache dans le cloud et sa relation avec des builds reproductibles).

Cependant, nous pensons qu'Earthly et des outils similaires (comme [Dagger.io](https://dagger.io)) sont prometteurs et résolvent de nombreuses problématiques des systèmes de CI/CD actuels tout en réduisant considérablement notre charge de maintenance, d'où ce choix technique.
:::
