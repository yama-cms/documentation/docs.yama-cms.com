# Configurer les fichiers de sortie

Yama CMS est conçue pour fonctionner de manière transparente avec différentes technologies, sans être spécifiquement liée à l'une d'entre elles. 
Son objectif est de **garantir une compatibilité et une interopérabilité maximales entre diverses solutions technologiques**, permettant ainsi une flexibilité et une adaptabilité accrues dans un environnement en constante évolution.
Nous souhaitons donner l'opportunité aux développeurs d'adopter des solutions technologiques qui correspondent le mieux à leurs besoins, sans être limités par des contraintes liées à une technologie particulière.

Toujours avec la même ligne directrice, nous avons fait le choix de rendre **vos contenus indépendants de la plateforme**.
Pour cette raison, l'ensemble des contenus administrés avec Yama CMS seront générés sous la forme de fichiers et envoyés dans votre dépôt de code :
- Menus => Fichiers au format JSON
- Taxonomies => Fichiers au format JSON
- Contenus => Fichier au format Markdown
- Données => Fichiers au format JSON

:::info
À n'importe quel moment vous pouvez arrêter d'utiliser Yama CMS, vous conserverez la possibilité de reconstruire votre site et gérer son hébergement comme bon vous semble.
:::

## Configuration

Pour les contenus donnant lieu à la génération de fichiers, vous avez la possibilité de configurer un répertoire de destination et un nom de fichier. 
Voici un exemple de configuration pour créer un menu de navigation :

- Code : `main`
- Répertoire de destination : `data/menus`
- Nom du fichier : `{code}.json`

![Image](/img/guide/outputs/configuration.png)

Vous pouvez déclarer un paramètre `{code}` qui sera automatiquement remplacé par la valeur renseignée dans le champ du formulaire prévu à cet effet.
Un fichier `data/menus/main.json` sera créé et versionné dans votre dépôt de code :

```json title="data/menus/main.json"
{
    "code": "main",
    "parent": null,
    "media": null,
    "translations": {
        "fr_FR": {
            "name": "Menu principal",
            "slug": null,
            "description": null,
            "url": null
        }
    },
    "children": [],
```

:::info
Pour plus de souplesse, l'application permet d'utiliser différents paramètres pour vos types de contenu.
Vous pouvez consulter notre [guide sur la création d'un type de contenu](/docs/guide/content-type#fichiers-de-sortie) pour obtenir la liste des paramètres.
:::
