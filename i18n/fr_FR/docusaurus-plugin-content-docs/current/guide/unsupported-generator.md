# Configurer un générateur de site statique non supporté

Yama CMS est conçu pour fonctionner de manière transparente avec différentes technologies, sans être spécifiquement lié à l'une d'entre elles. 
Son objectif est de **garantir une compatibilité et une interopérabilité** maximales entre diverses solutions technologiques, permettant ainsi une flexibilité et une adaptabilité accrues dans un environnement en constante évolution. 
Nous souhaitons donner l'opportunité aux **développeurs d'adopter des solutions technologiques qui correspondent le mieux à leurs besoins**, sans être limités par des contraintes liées à une technologie particulière.

Vous trouverez dans ce guide les informations pour **rendre compatible n'importe quel générateur de site statique** n'étant pas encore supporté officiellement par la plateforme.
Il suffira de réaliser les opérations suivantes :
1. [Créer un manifeste Earthly pour pouvoir "build" & "deploy" votre site statique](/docs/guide/unsupported-generator#cr%C3%A9er-un-manifeste-pour-un-g%C3%A9n%C3%A9rateur-de-site-statique-non-support%C3%A9)
2. [Personnaliser les fichiers de sortie pour prendre en compte les spécificités de votre générateur de site statique](/docs/guide/unsupported-generator#personnaliser-les-fichiers-de-sortie-pour-un-g%C3%A9n%C3%A9rateur-site-statique-non-support%C3%A9)

## Créer un manifeste pour un générateur de site statique non supporté

Les manifestes Earthly sont un croisement entre Dockerfiles et Makefiles et sont donc compatibles avec n'importe quel générateur. Il suffit de définir les entrées et sorties (`COPY`, `SAVE ARTIFACT`) et les commandes à exécuter (`RUN`).

Les Earthfiles ont besoin au minimum de trois `targets` :
- _build_ (construire le site via un générateur)
- _deploy-production_ et _deploy-preview_ (envoyer les fichiers générés)

:::info
Nous proposons une template qui inclut les targets nécessaire au déploiement, disponible [ici](https://gitlab.com/yama-cms/templates/earthly/-/blob/main/manifests/earthly/do-it-yourself/Earthfile).
:::

À titre d'example, voici un manifeste Earthfile simplifié :

```Earthfile
VERSION 0.7 # https://docs.earthly.dev/docs/earthfile#version
FROM python:3

ARG --global build_dir="public"

# Build the site
build:
    FROM debian:stable

    RUN mkdir -p $build_dir
    RUN echo "Run any command you need here!" > $build_dir/index.html

    SAVE ARTIFACT $build_dir build_result AS LOCAL $build_dir

deploy-production:
    FROM python:3

    COPY +build/build_result ./to-upload

    RUN echo "(fake) site upload!"
    RUN ls ./to-upload
```

Tous nos Earthfiles utilisent la même target pour _upload_ et attendent que la target `+build` définisse un _artifact_ nommé `build_result`.

Pour ce qui est des commandes à lancer, elles vont dépendre de votre générateur : vous devez utiliser `COPY ...` pour copier les éléments nécessaire au build de votre site, puis utiliser `RUN ...` pour exécuter les différentes étapes de votre build. Par exemple, voici la target `+build` pour `hugo`, en utilisant l'image [klakegg/hugo](https://hub.docker.com/r/klakegg/hugo):
```
build:
    FROM klakegg/hugo:alpine-ci           # Define the base image

    ENV HUGO_ENV=production
    COPY . .                              # Copy the current directory's content to the image
    RUN hugo --destination="$build_dir"   # Build the site

    SAVE ARTIFACT $build_dir build_result AS LOCAL $build_dir   # Save the result
```

Cet exemple est très simple et basique parce qu'Hugo est par défaut très simple à utiliser.
Pour des _build systems_ plus compliqués, il faudra peut-être lancer plus de commandes, lancer une suite de tests unitaires, installer les dépendances nécessaires (`npm install`...), etc.

Pour aller plus loin, nous vous recommandons de lire la section [Learn the basics](https://docs.earthly.dev/basics/part-1-a-simple-earthfile) de la documentation d'Earthly, et d'adapter notre template Do-It-Yourself [disponible ici](https://gitlab.com/yama-cms/templates/earthly/-/blob/main/manifests/earthly/do-it-yourself/Earthfile).

## Personnaliser les fichiers de sortie pour un générateur site statique non supporté

Les contenus administrés avec Yama CMS sont générés sous la forme de fichiers et envoyés dans votre dépôt de code. 
Certains **générateurs de sites statiques sont développés pour consommer directement des fichiers Markdown** mais d'autres nécessitent d'être légèrement adaptés.

Vous pouvez consulter notre page au sujet des [générateurs de sites statiques officiellement supportés](/docs/generators) par la plateforme.
Vous obtiendrez probablement des informations utiles dans une démarche de configuration de votre générateur de site statique.
Dans ce chapitre, nous nous focaliserons sur les fonctionnalités proposées par Yama CMS pour faciliter cette adaptation.

### Gestion de la destination des fichiers pour votre générateur de site statique

Pour les contenus donnant lieu à la génération de fichiers, vous avez la possibilité de configurer un répertoire de destination et un nom de fichier. 
Voici un exemple de configuration pour gérer la destination des fichiers Markdown pour le **générateur de site statique Hugo** :

![Image](/img/guide/unsupported-generator/form-post-config.png)

Vous pouvez déclarer des paramètres qui seront automatiquement remplacés lors de la génération du fichier.
Rendez-vous sur la page de [création d'un type de contenu](/docs/guide/content-type) pour obtenir la liste des paramètres afin de rendre dynamique le chemin de sortie de votre fichier.

### Gestion des métadonnées exploitées par votre générateur de site statique

#### Réaliser un mappage des métadonnées

Pour les fichiers Markdown, un ensemble de métadonnées au format YAML sont ajoutées automatiquement en début de fichier :

```yaml
locale: fr_FR
slug: azerty
permalink: /fr_FR/2023-05-16/azerty
reference: 6463a9e38bd01
parent: null
date: '2023-05-16'
title: azerty
description: null
excerpt: null
tags: []
media: null
catalogs: null
```

Vous avez la possibilité de mettre en place un mappage pour renommer certaines propriétés en fonction du générateur de site statique utilisé.
Par exemple, [Hugo](https://gohugo.io/) nécessite de renommer la propriété `permalink` par `url`:

![Image](/img/guide/unsupported-generator/frontmatter-mapping.png)

#### Ajouter des informations aux métadonnées

L'intégration d'un [JSON Schema](https://json-schema.org/) permet de définir des données structurées qui seront associées aux fichiers Markdown et accessibles dans vos métadonnées.
La définition d'un **JSON Schema offre la possibilité d'ajouter des valeurs par défaut** et/ou de générer un formulaire personnalisé directement dans l'interface d'édition d'un contenu.
Pour plus d'informations, vous pouvez consulter notre guide sur les [formulaires personnalisés](/docs/guide/schemas).

![Image](/img/guide/content-type/schemas.png)

Par exemple, pour le **générateur de site statique Jekyll** une propriété `layout` doit être renseignée.
Un schéma peut être associé à un [type de contenu](/docs/guide/content-type.md) pour ajouter aux métadonnées cette information demandée par Jekyll :

```json
{
  "properties": {
    "layout": {
      "type": "string",
      "enum": ["post", "page"],
      "default": ["post"]
    }
  }
}
```

## Automatiser la configuration pour votre générateur de site statique

Yama CMS utilise un système de "presets" pour s'interfacer de manière native avec les **principaux générateurs de sites statiques**.
Cette fonctionnalité offre également la possibilité d'[automatiser la personnalisation des fichiers de sortie](/docs/guide/presets) pour un générateur de site statique non supporté.
