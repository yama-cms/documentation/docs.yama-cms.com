# Générer un fichier de traductions

Le multilingue est intégré de manière native au sein de Yama CMS, rien de plus simple pour ajouter le support d'une nouvelle langue pour un site. Rendez-vous dans la configuration de votre site pour ajouter la locale "en_US" :

![Image](/img/use-case/translations/sidebar.png) 

:::info
Vous pouvez voir et modifier la langue en cours d'édition depuis la barre latérale.
:::

## Création d'un type de donnée

La première étape est la création d'un nouveau type de donnée, il sera nommé **Traductions** avec le code `translations` :

![Image](/img/use-case/translations/type.png)

### Ajout d'un JSON Schema

Depuis l'onglet **Schémas**, nous allons définir un schéma permettant de renseigner une clé de traduction :

```js
{
  "properties": {
    "key": {
      "type": "string",
      "title": "Clé"
    }
  }
}
```

### Ajout d'un JSON Schema multilingue

Cette fonctionnalité permet aux utilisateurs de renseigner des informations pour une structure de données personnalisées en fonction des langues supportées par votre site.
Pour plus d'informations, suivez notre [guide pour la gestion de formulaires personnalisés](/docs/guide/schemas).

Depuis l'onglet **Schémas (multilingue)**, nous implémentons un schéma pour définir un message de traduction :

```js
{
  "properties": {
    "message": {
      "type": "string", 
      "title": "Message"
    }
  }  
}
```

:::info
Dans la majorité de cas, la définition d'un schéma multilingue pour votre "fallback locale" est suffisante. Au besoin, vous pourrez également définir une structure de données différente en fonction des locales.
:::

Pour finir, nous ajouterons un **UI Schema** pour modifier le rendu du formulaire afin d'exploiter un champ "textarea" pour renseigner le message de traduction : 

```js
{
  "message": {
    "ui:widget": "textarea"
  }
}
```

## Création d'une traduction

Votre nouveau type de donnée est dorénavant accessible depuis la barre latérale. Lors de la création d'une nouvelle donnée, voici le formulaire qui sera proposé :

![Image](/img/use-case/translations/form.png)

### Exemple du fichier de sortie

```js title="data/translations.json"
[
  {
    "key": "form.contact.subject",
    "translations": {
      "fr_FR": {
        "message": "Sujet"
      },
      "en_US": {
        "message": "Subject"
      }
    }
  }
]
```

## Exploiter le fichier

```js title="src/lib/i18n.js"
import translations from "../../data/translations.json";

export default function trans(key, locale) {
  return translations.map(item => item.key === key ? item.translations[locale].message : null)
}
```

```js title="Contact.js"
import trans from "../../src/lib/i18n";

export default function Contact({ page }) {
    return (
        <form>
          <div>
            <label htmlFor="subject">
              {trans('form.contact.subject', page.locale)}
            </label>
            <textarea className="subject"></textarea>
          </div>
        </form>
    );
}
```
