# Uploader des fichiers via un JSON Schema

:::caution
Un fichier encodé au format base64 va entraîner une augmentation de **33%** à **300%** comparativement à sa taille initiale, source [MDN](https://developer.mozilla.org/fr/docs/Glossary/Base64).
:::

## Création d'un JSON Schema

La première étape consiste à créer un [JSON Schema](https://json-schema.org/), vous pouvez réaliser cette opération lors de la configuration d'un type de contenu ou donnée.
Votre schéma devra définir une propriété de type `data-url` qui permettra de sélectionner un fichier à partir du formulaire qui sera généré.

Pour notre exemple, nous allons configurer un type de contenu **Page** pour permettre l'ajout de fichiers ne devant pas être accessibles depuis la médiathèque :

```js title="JSON Schema"
{
  "properties": {
    "files": {
      "type": "array",
      "title": "Fichiers",
      "items": {
        "properties": {
          "name": {
            "type": "string",
            "title": "Nom du fichier"
          },
          "base64": {
            "type": "string",
            "format": "data-url"
          }
        }
      }
    }
  }
}
```

![Image](/img/use-case/schema-files/page.png)

### Contraintes sur les fichiers

Vous pouvez ajouter des contraintes afin de définir les types de fichiers autorisés par l'intermédiaire d'un **UI Schema** :

```js title="UI Schema"
{
  "files": {
    "items": {
      "base64": {
        "ui:options": {
          "accept": [".pdf", ".svg"]
        }
      }
    }
  }
}
```

:::caution
La validation d'un [UI Schema](https://github.com/ui-schema/ui-schema) est réalisée seulement au niveau du navigateur.
:::

## Création d'une page de contenu

Nous pourrons observer depuis l'onglet **Personnalisation** le formulaire correspondant au JSON Schema préalablement configuré :

![Image](/img/use-case/schema-files/custom-page.png)

Concernant le fichier de sortie, vous pourrez observer la génération d'une propriété `files` qui contient la liste des fichiers encodés en base64 :

![Image](/img/use-case/schema-files/front-matter.png)

## Exploiter les fichiers

```js title="Page.js"
import Svg from "../Page/Svg"
import Pdf from "../Page/Pdf"

export default function Page({ page }) {
    return (
        <>
            <h1>{page.title}</h1>
            {
                page.files?.map((file, index) => {
                    const fileType = file.base64.split(";").shift()
                    switch (fileType) {
                        case "data:image/svg+xml":
                            return <Svg key={index} file={file} />
                        case "data:application/pdf":
                            return <Pdf key={index} file={file} />
                        default:
                            return <span>Undefined file type</span>
                    }
                })
            }
        </>
    )
}
```

```js title="Svg.js"
export default function Svg({ file }) {
    return (
        <img src={file.base64} alt={file.name}/>
    )
}
```

```js title="Pdf.js"
export default function Pdf({ file }) {
    return (
        <object data={file.base64} type="application/pdf" width="100%" height="500px">
            <p>Unable to display PDF file. <a href={file.base64}>Download</a> instead.</p>
        </object>
    )
}
```
