# Implémenter un parseur Markdown

## Utilisation du parseur Markdown-it

Voici un exemple d'installation des dépendances et de configuration du parseur Javascript [Markdown-it](https://github.com/markdown-it/markdown-it) permettant la prise en charge des différentes fonctionnalités de Yama CMS :

```
npm install markdown-it markdown-it-abbr markdown-it-footnote markdown-it-deflist gray-matter
```

```js
import matter from "gray-matter";
import MarkdownIt from "markdown-it";
import MarkdownItAbbr from "markdown-it-abbr";
import MarkdownItFootnote from "markdown-it-footnote";
import markdownItDeflist from "markdown-it-deflist";

const markdownToHtml = () => {
    const fileContent = matter('---\ntitle: Home\nslug: homepage`\n---\n# Title of page')
    const markdown = new MarkdownIt({ html: true })
        .use(MarkdownItAbbr)
        .use(MarkdownItFootnote)
        .use(markdownItDeflist)

    return {
        frontmatter: fileContent.data,
        content: markdown.render(fileContent.content),
    }
}


//  {
//    frontmatter: { title: 'Home', slug: 'homepage`' },
//    content: '<h1>Title of page</h1>\n'
//  }
```
:::caution
Si vous décidez d'intégrer un parseur **Markdown**, nous conseillons l'utilisation de la librairie [Contentlayer](https://www.contentlayer.dev).
:::

## Configuration du parseur de ContentLayer

**ContentLayer** intègre nativement le parseur [Remark](https://github.com/remarkjs/remark), il est possible de configurer l'outil dans la fonction `makeSource` du fichier `contentlayer.config.js`. Nous partons du principe que vous avez déjà étudié et configuré Contentlayer, voici les dépendances à installer pour bénéficier des fonctionnalités proposées par Yama CMS :

```
npm install remark-gfm remark-frontmatter remark-definition-list remark-rehype rehype-stringify rehype-raw
```

```js title="contentlayer.config.js"
import remarkGfm from 'remark-gfm'
import remarkFrontmatter from 'remark-frontmatter'
import remarkParse from 'remark-parse'
import remarkRehype from 'remark-rehype/lib/index.js'
import rehypeStringify from 'rehype-stringify'
import rehypeRaw from 'rehype-raw'
import remarkDefinitionList, { defListHastHandlers } from 'remark-definition-list'

// ...

export default makeSource({
    // ...
    markdown: (builder) => {
      builder.use(remarkParse)
      builder.use(remarkFrontmatter)
      builder.use(remarkGfm)
      builder.use(remarkDefinitionList)
      builder.use(remarkRehype, {allowDangerousHtml: true, handlers: {...defListHastHandlers}})
      builder.use(rehypeRaw)
      builder.use(rehypeStringify)
    }
})
```

:::caution
Le plugin `remark-abbr` n'est malheureusement plus mis à jour et ne fonctionne pas avec les dernières versions de l'outil.
:::
