# Implémenter un fil d'Ariane

## Prérequis

- Avoir créé et défini une page d'accueil pour le site

## Création d'une page

Nous allons dans un premier temps créer une nouvelle page. Ensuite, depuis l'onglet **Configuration** il faudra sélectionner une page parente et enregistrer les modifications :

![Image](/img/use-case/breadcrumb/parent.png) 


Le code permettant la gestion de notre fil d'Ariane devra s'appuyer sur la valeur `parent`. Par défaut, si aucune page n'est renseignée `parent` vaut `null`. Dans notre cas, `parent` correspond à la référence de la page d'accueil :

```js title="glossaire.fr_FR.md"
locale: fr_FR
slug: glossaire
permalink: /fr_FR/glossaire
reference: glossaire
// highlight-next-line
parent: 6454c5be7297c
date: '2023-05-05'
title: Glossaire
description: null
excerpt: null
tags: []
media: null
catalogs: null
category: []
```

## Affichage du fil d'Ariane

Voici un exemple d'implémentation React permettant d'afficher d'un fil d'Ariane :

```js title="Breadcrumb.js"
export default function BreadCrumb({ currentPage, pages }) {
    const parentPages = []

    const getParentPage = (currentPage, pages) => {
        pages.map(page => {
            if (page.reference === currentPage.parent) {
                parentPages.push(page)
                getParentPage(page, pages)
            }
        })
    }

    getParentPage(currentPage, pages)
    const parentPagesOrdered = parentPages.reverse()

    return (
        parentPages.length > 0 ?
        <ul>
            {
                parentPagesOrdered.map((page, index) =>
                    <li key={index}> 
                        <a href={page.permalink}>
                            {page.title} /
                        </a>
                    </li>
                )
            }
            <li>{currentPage.title}</li>
        </ul>
        :
        <></>
    )
}
```
