# Intégrer un système de glossaire

## Création d'une nouvelle taxonomie

Dans un premier temps, nous allons créer une nouvelle taxonomie nommée **Glossaire** avec le code `glossary`. Cette taxonomie permettra de renseigner l'ensemble des catégories (taxons) de notre glossaire.

## Création de taxons

Ensuite, nous allons ajouter plusieurs taxons à la taxonomie **Glossaire** : 

- Infrastructure
- Technologies
- Définitions

### Exemple du fichier de sortie

```json title="data/taxonomies/glossary.json"
{
    "code": "glossary",
    "parent": null,
    "media": null,
    "children": [
        {
            "code": "6454d25fb0b87",
            "parent": "glossary",
            "media": null,
            "children": [],
            "translations": {
                "fr_FR": {
                    "name": "Infrastructure",
                    "slug": "glossaire\/infrastructure",
                    "description": null
                }
            }
        },
        {
            "code": "6454d26dd16a7",
            "parent": "glossary",
            "media": null,
            "children": [],
            "translations": {
                "fr_FR": {
                    "name": "Technologies",
                    "slug": "glossaire\/technologies",
                    "description": null
                }
            }
        },
        {
            "code": "6454d2c1ab156",
            "parent": "glossary",
            "media": null,
            "children": [],
            "translations": {
                "fr_FR": {
                    "name": "Definitions",
                    "slug": "glossaire\/definitions",
                    "description": null
                }
            }
        }
    ],
    "translations": {
        "fr_FR": {
            "name": "Glossaire",
            "slug": "glossaire",
            "description": null
        }
    }
}
```

## Création d'un nouveau type de contenu

Nous allons créer un nouveau type de contenu qui sera nommé **Termes** et qui devra être associé à notre taxonomie **Glossaire** :

![Image](/img/use-case/glossary/add-taxonomy.png)

## Création d'un terme

Pour cet exemple, nous allons renseigner les informations suivantes : 

- Un titre
- Un contenu
- Un ou des taxons

### Exemple du fichier de sortie

```js title="content/terms/docker.fr_FR.md"
---
locale: fr_FR
slug: docker
permalink: /glossaire/docker
reference: 6454c9b8743fc
parent: null
date: '2023-05-05'
title: Docker
description: null
excerpt: null
tags: []
media: null
catalogs: null
//highlight-start
glossary:
    - Infrastructure
    - Definitions
//highlight-end
---
Docker est une plateforme permettant de lancer certaines applications dans des conteneurs logiciels lancé en 2013. Wikipédia
```

:::info
Nous pouvons constater l'ajout d'une nouvelle propriété nommée `glossary` correspondant au code spécifié pour la taxonomie **Glossaire** avec les taxons ayant été sélectionnés.
:::

## Création d'une page de listing

Une fois nos termes renseignés, il faudra créer un nouveau type de contenu **Page**. Bien entendu, le titre utilisé pour cette page sera "Glossaire".

Cette page permettra de lister l'ensemble des catégories de notre taxonomie **Glossaire**. Ensuite, nous listerons pour chaque catégorie les contenus de type **Termes** pour finaliser l'implémentation de notre glossaire.

### Implémentation de la page de listing

```js title="Glossary.js"
import Link from 'next/link'
import { allTerms as terms } from 'contentlayer/generated'
import taxons from '../../data/taxonomies/glossary.json'

export default function Glossary({ page }) {
    return (
        <>
            <h1>{ page?.title }</h1>
            <ul>
                {
                    taxons?.children.map((taxon, index) =>
                        <li key={index}>
                            {taxon?.translations[page.locale].name}
                            <ul>
                                {
                                    terms?.map((term) =>
                                        term.glossary.map((glossary, index) =>
                                            glossary === taxon.translations[page.locale].name && <li key={index}><Link href={term?.permalink}>{term?.title}</Link></li>
                                        )
                                    )
                                }
                            </ul>
                        </li>
                    )
                    
                }
            </ul>
        </>
    )
}
```
