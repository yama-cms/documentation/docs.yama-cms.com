import React from 'react';
import YamaFooter from '../../components/Footer/Footer';

function Footer() {
  return <YamaFooter />;
}
export default React.memo(Footer);
