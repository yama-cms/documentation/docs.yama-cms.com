import React from 'react';
// Import the original mapper
import MDXComponents from '@theme-original/MDXComponents';
import Features from '@site/src/components/Features/Features';
import Benchmark from '@site/src/components/Benchmark/Benchmark';

export default {
  // Re-use the default mapping
  ...MDXComponents,
  Features,
  Benchmark
};