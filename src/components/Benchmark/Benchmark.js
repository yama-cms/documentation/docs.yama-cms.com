import React from "react";
import styles from "./Benchmark.module.css";

import { useState, useEffect } from "react";
import { Chart } from "chart.js/auto";
import benchmarkData from "@site/src/data/benchmark.json";
import { translate } from "@docusaurus/Translate";

Chart.defaults.font.family = "Poppins, sans-serif";

const PAGES_COUNT = [10, 50, 100, 200, 500, 1000];
const INITIAL_PAGE_COUNT = 10;
const CANVAS_ID = "canvas";

/** @param {number} page */
const createDatasets = (page) => {
    const dataSorted = benchmarkData.sort((a, b) => a.pages[page] - b.pages[page]);
    return [
        {
            barPercentage: 0.6,
            data: dataSorted,
            backgroundColor: dataSorted.map(a => `rgba(${a.color}, 0.15`),
            borderColor: dataSorted.map(a => `rgba(${a.color}, 1`),
            borderWidth: 1,
            parsing: {
                xAxisKey: `pages.${page}`,
                yAxisKey: "name",
            }
        }
    ]
};

const cfg = {
    type: "bar",
    data: { datasets: createDatasets(INITIAL_PAGE_COUNT) },
    options: {
        indexAxis: "y",
        plugins: {
            tooltip: {
                padding: 12,
                bodySpacing: 12,
                backgroundColor: "rgba(0, 0, 0)",
                usePointStyle: true,
                titleMarginBottom: 12,
                footerFont: {
                    weight: "normal"
                },
                callbacks: {
                    labelPointStyle: () => {
                        return {
                            pointStyle: 'triangle'
                        }
                    },
                    /** @param {Object} context */
                    labelColor: (context) => {
                        return {
                            borderColor: `rgba(${context.raw.color})`,
                            backgroundColor: `rgba(${context.raw.color})`,
                        }
                    },
                    /** @param {Array} tooltipItems */
                    afterTitle: (tooltipItems) => `Version : ${tooltipItems[0].raw.version}`,
                    /** @param {Object} context */
                    label: (context) => `${translate({id: "benchmark.pageGenerate"})} : ${context.chart.config._config.pageCount}`,
                    /** @param {Array} tooltipItems */
                    footer: (tooltipItems) => {
                        const buildtimeMs = tooltipItems[0].parsed.x;
                        const buildTimeSeconds = buildtimeMs / 1000;
                        return `${translate({id: "benchmark.buildTime"})} : ${buildTimeSeconds} ${translate({id: "benchmark.seconds"})}`;
                    }
                }
            },
            legend: {
                display: false,
            }
        },
        scales: {
            x: {
                title: {
                    display: true,
                    text: translate({id: "benchmark.xAxisTitle"}),
                    padding: 20,
                },
                ticks: {
                    /** @param {number} value */
                    callback: (value) => `${value}ms`
                }
            }
        }
    }
}

export default function Benchmark() {
    const [currentPageCount, setCurrentpageCount] = useState(INITIAL_PAGE_COUNT);

    useEffect(() => {
        /** @type {*} */
        const config = ({ ...cfg, data: {datasets: createDatasets(currentPageCount)}, pageCount: currentPageCount });
        const chart = new Chart(CANVAS_ID, config);
        
        return () => chart.destroy();
    }, [currentPageCount]);

    
    return (
        <>
        <div className={styles.optionsWrapper}>
            <div className={styles.optionsTitle}>{translate({id: "benchmark.simulateGeneration"})}</div>
            <div className={styles.optionsContainer}>
                {PAGES_COUNT.map((pageCount, index) => {
                    return (
                        <span 
                            className={`${styles.options} ${currentPageCount == pageCount ? styles.optionsActive : ""}`}
                            key={index} 
                            onClick={() => setCurrentpageCount(pageCount)}
                        >
                            {pageCount}
                        </span>
                    )
                })}
            </div>
        </div>
        <div className={styles.wrapper}>
            <canvas
                id={CANVAS_ID}
                width="400"
                height="400"
                aria-label="Benchmark static site generators"
                role="img"
            />
        </div>
        </>
    );
}
