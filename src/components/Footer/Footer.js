import React from "react";
import styles from './Footer.module.css';
import Link from "@docusaurus/Link";
import LogoFooter from "@site/static/img/home/yama-logo-text.svg"

export default function Footer() {
    return (
        <footer className={styles.footer}>
            <div className={styles.logoContainer}>
                <Link className={styles.logo} href="https://yama-cms.com">
                  <LogoFooter />
                </Link>
            </div>
            <span className={styles.copyright}>Built with Docusaurus</span>
            <div className={styles.links}>
                <Link href="https://yama-cms.com/fr/nous-contacter/">
                  Nous contacter
                </Link>
                <Link href="https://yama-cms.com/fr/mentions-legales/">
                  Mentions légales
                </Link>
            </div>
        </footer>
    );
}
