import React from 'react';
import styles from './Cards.module.css';
import Card from './Card';

const cardsList = [
    {
        description: "Débuter rapidement avec Yama CMS !",
        link: "/docs/quick-start",
        position: "right"
    },
    {
        description: "Pourquoi utiliser Yama CMS ?",
        link: "https://yama-cms.com/",
        position: "left"
    }
]

export default function Cards() {
    return (
        <div className={styles.cardsWrapper}>
            {cardsList.map((card, index) =>
                <Card
                    key={index}
                    description={card.description}
                    link={card.link}
                    position={card.position}
                />
            )}
        </div>
    )
}
