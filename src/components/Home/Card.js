import React from 'react';
import Link from '@docusaurus/Link';
import styles from './Card.module.css';

export default function Card({description, link, position}) {
    return (
        <span className={styles.card}>
            {description}
            <button className={position === "left" ? styles.buttonLeft : styles.buttonRight}>
                <Link className={styles.position} href={link}>
                    Voir
                </Link>
            </button>
        </span>
    )
}