import React from "react";
import styles from './Feature.module.css';

import unified from 'unified'
import remarkParse from 'remark-parse'
import remarkGfm from 'remark-gfm'
import remarkRehype from 'remark-rehype'
import rehypeStringify from 'rehype-stringify'


let markdownToHtml = (mdStr) => unified().use(remarkParse).use(remarkGfm).use(remarkRehype).use(rehypeStringify).processSync(mdStr)

let statusTranslations = {
    "fr_FR": {
        "Complete": "Terminé",
        "Future": "Futur",
        "Planned (beta)": "Prévu (Béta)",
    }
}

export default function Feature({ data, locale }) {
    return (
        <div className={styles.card}>
            <h3>
                {data.translations[locale].title}
                <span data-status={data.status} className={styles.label}>
                    {statusTranslations[locale][data.status] || data.status }
                </span>
            </h3>
            <p dangerouslySetInnerHTML={{ __html: markdownToHtml(data.translations[locale].description) }}></p>
        </div>
    );
}
  