import React from "react";
import styles from './Features.module.css';
import Feature from "./Feature";
import featuresData from "@site/src/data/features.json";

// TODO: read locale from docusaurus
const locale = 'fr_FR'


// Declare categories and their order here
const featuresByCategory = new Map([
    ["main", []],
    ["content", []],
    //["organisation", []],
    ["beyond", []],
    ["deployment", []],
    ["network", []],
    ["misc", []],
]);


const categoryTranslations = {
    fr_FR: {
        "main": 'Plateforme',
        "content": 'Contenu',
        "beyond": 'Beyond static',
        "organisation": 'Organisation',
        "deployment": 'Déploiement',
        "network": 'Réseau',
        "misc": 'Fonctionnalités additionnelles',
    }
}


// Go through each feature and add it to the map
featuresData.forEach(feature => {
    const cat = featuresByCategory.get(feature.category)

    if (cat) cat.push(feature)
    else throw (`Unknown category "${feature.category}". Please add it to the map first!`)
})



export default function Features() {
    return (
        <div>
            {
                [...featuresByCategory.entries()].map(([categoryName, features]) => {
                    return (
                        <div className={styles.category}>
                            <h2>{ categoryTranslations[locale][categoryName] }</h2>

                            <div className={styles.cardContainer}>
                                { features.map((feature, index) => <Feature key={index} data={feature} locale={locale} />) }
                            </div>
                        </div>
                    );
                })
            }
        </div>
    );
}
