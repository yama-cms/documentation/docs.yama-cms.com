import React from 'react';
import useDocusaurusContext from '@docusaurus/useDocusaurusContext';
import Layout from '@theme/Layout';
import ThemedImage from '@theme/ThemedImage';
import useBaseUrl from '@docusaurus/useBaseUrl';

import styles from './index.module.css';
import logoTop from '@site/static/img/home/yama-top.png';
import LogoSavoie from '@site/static/img/home/logo-savoie.svg';
import Cards from '../components/Home/Cards';

export default function Home() {
  const {siteConfig} = useDocusaurusContext();
  return (
    <Layout
      description="Documentation about Yama CMS Application"
    >
      <div className={styles.container}>
        <div className={styles.logoContainer}>
            <img className={styles.logoTop} src={logoTop} alt="Logo Yama CMS"/>
            <ThemedImage
                className={styles.logoBottom}
                alt="Logo Yama CMS"
                sources={{
                    light: useBaseUrl('/img/home/yama-bottom.png'),
                    dark: useBaseUrl('/img/home/yama-bottom.png')
                }}
            />
        </div>
        <div className={styles.catchPhrase}>
            <img className={styles.logoTitle} src={useBaseUrl('/img/home/frenchtech.png')} alt="Logo French Tech"/>
            <h2 className={styles.topTitle}>Le CMS made in Savoie</h2>
        </div>
        <Cards />
      </div>
    </Layout>
  );
}
