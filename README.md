# Website

This website is built using [Docusaurus 2](https://docusaurus.io/), a modern static website generator.

## Installation

- Live reload for **FR** documentation : `npm run start`
- Live reload for **EN** documentation : `npm run start-en`

> Can't see at the same time the **FR** and **EN** documentation version with npm serv cause it's one SPA per locale.

See full documentation (all locales in one App) : `npm run build`

## License

The code for this project is under the [MIT license](./LICENSE). 
The documentation itself (e.g., .md files in the /docs and /i18n directories, and relevant files in /data) is under the [CC-BY-SA-4.0 license](./LICENSE-docs), at the exception of code examples or snippets within the documentation which are under the MIT license.
