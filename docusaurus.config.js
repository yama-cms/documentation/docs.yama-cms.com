// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

const lightCodeTheme = require('prism-react-renderer/themes/github');
const darkCodeTheme = require('prism-react-renderer/themes/dracula');

/** @type {import('@docusaurus/types').Config} */
const config = {
    plugins: [
        [
            require.resolve("@cmfcmf/docusaurus-search-local"),
            {
                language: ["fr", "en"]
            },
        ],
    ],
    title: 'Yama CMS beyond static - Made in Savoie',
    tagline: 'beyond static',
    favicon: 'img/favicon.ico',

    // Set the production url of your site here
    url: 'https://docs.yama-cms.com',
    // Set the /<baseUrl>/ pathname under which your site is served
    // For GitHub pages deployment, it is often '/<projectName>/'
    baseUrl: '/',

    // GitHub pages deployment config.
    // If you aren't using GitHub pages, you don't need these.
    organizationName: 'facebook', // Usually your GitHub org/user name.
    projectName: 'docusaurus', // Usually your repo name.

    onBrokenLinks: 'throw',
    onBrokenMarkdownLinks: 'warn',

    // Even if you don't use internalization, you can use this field to set useful
    // metadata like html lang. For example, if your site is Chinese, you may want
    // to replace "en" with "zh-Hans".
    i18n: {
        defaultLocale: 'fr',
        locales: ['fr', 'en'],
        localeConfigs: {
            fr: {
                label: 'Français',
                direction: 'ltr',
                htmlLang: 'fr-FR',
                calendar: 'gregory',
                path: 'fr_FR',
            },
            en: {
                label: 'English',
                direction: 'ltr',
                htmlLang: 'en-US',
                calendar: 'gregory',
                path: 'en_US',
            },
        },
    },

    presets: [
        [
            'classic',
            /** @type {import('@docusaurus/preset-classic').Options} */
            ({
                docs: {
                    sidebarPath: require.resolve('./sidebars.js'),
                },
                theme: {
                    customCss: require.resolve('./src/css/custom.css'),
                },
            }),
        ],
    ],

    themeConfig:
        /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
        ({
            metadata: [
                {name: 'og:image', content: 'https://docs.yama-cms.com/img/yama-social-card.jpg'},
                {name: 'twitter:image', content: 'https://docs.yama-cms.com/img/yama-social-card.jpg'},
                {property: 'og:image', content: 'https://docs.yama-cms.com/img/yama-social-card.jpg'},
                {property: 'twitter:image', content: 'https://docs.yama-cms.com/img/yama-social-card.jpg'},
            ],
            // This would become <meta name="keywords" content="cooking, blog"> in the generated HTML
            // Replace with your project's social card
            image: 'img/docusaurus-social-card.jpg',
            navbar: {
                title: 'Yama CMS',
                logo: {
                    alt: 'Yama CMS beyond static',
                    src: 'img/yama-cms-logo.svg',
                },
                items: [
                    {
                        to: 'docs/quick-start',
                        label: 'Quick Start',
                        activeBasePath: 'docs/quick-start',
                    },
                    {
                        to: 'docs/contents',
                        label: 'Contenus',
                        activeBasePath: '/docs/contents',
                    },
                    {
                        to: 'docs/generators',
                        label: 'Générateurs',
                        activeBasePath: 'docs/generators',
                    },
                    {
                        to: 'docs/benchmark',
                        label: 'Benchmark',
                        activeBasePath: 'docs/benchmark',
                    },
                    {
                        to: 'docs/features-roadmap',
                        label: 'Features / Roadmap',
                        activeBasePath: 'docs/features-roadmap',
                    },
                    {
                        to: 'docs/concepts',
                        label: 'Concepts',
                        activeBasePath: 'docs/concepts',
                    },
                    {
                        type: 'search',
                        position: 'right'
                    },
                    {
                        href: 'https://gitlab.com/yama-cms/documentation/docs.yama-cms.com',
                        position: 'right',
                        className: 'header-gitlab-link',
                        'aria-label': 'Gitlab repository',
                    },
                    /*{
                        type: 'localeDropdown',
                        position: 'right',
                    },*/
                ],
            },
            colorMode: {
                defaultMode: 'light',
                respectPrefersColorScheme: false
            },
            prism: {
                theme: lightCodeTheme,
                darkTheme: darkCodeTheme,
                magicComments: [
                    {
                        className: 'theme-code-block-highlighted-line',
                        line: 'highlight-next-line',
                        block: { start: 'highlight-start', end: 'highlight-end' },
                    },
                    {
                        className: 'code-block-error-line',
                        line: 'error-next-line',
                        block: { start: 'error-start', end: 'error-end' },
                    },
                    {
                        className: 'code-block-validate-line',
                        line: 'validate-next-line',
                        block: { start: 'validate-start', end: 'validate-end' },
                    },
                ],
            },
        }),
};

module.exports = config;
